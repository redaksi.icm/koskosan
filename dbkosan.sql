-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 12 Apr 2020 pada 19.52
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbkosan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_booking`
--

CREATE TABLE `tb_booking` (
  `no_booking` varchar(255) NOT NULL,
  `id_kos_kontrakan` varchar(255) DEFAULT NULL,
  `nama_sesuai_ktp` varchar(100) DEFAULT NULL,
  `alamat_sesuai_ktp` text DEFAULT NULL,
  `no_telpon` varchar(50) DEFAULT NULL,
  `no_whatsapp` varchar(50) DEFAULT NULL,
  `no_ktp` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `tgl_booking` timestamp NULL DEFAULT NULL,
  `status_booking` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_booking`
--

INSERT INTO `tb_booking` (`no_booking`, `id_kos_kontrakan`, `nama_sesuai_ktp`, `alamat_sesuai_ktp`, `no_telpon`, `no_whatsapp`, `no_ktp`, `jenis_kelamin`, `email`, `tgl_booking`, `status_booking`) VALUES
('BK12042020020046', 'PO820201355KN', 'Raisa', 'padang', '13456789', '6234567890', '38746237463', 'Laki-laki', 'surus@gmil.com', '2020-04-11 19:01:12', 'Selesai');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kos_kontrakan`
--

CREATE TABLE `tb_kos_kontrakan` (
  `id_kos_kontrakan` varchar(255) NOT NULL,
  `id_pemilik` varchar(255) DEFAULT NULL,
  `nama_kos_kontrakan` varchar(50) DEFAULT NULL,
  `alamat_kos_kontrakan` text DEFAULT NULL,
  `no_telpon_kos_kontrakan` varchar(50) DEFAULT NULL,
  `ket_kos_kontrakan` text DEFAULT NULL,
  `gambar_kos_kontrakan1` text DEFAULT NULL,
  `gambar_kos_kontrakan2` text DEFAULT NULL,
  `gambar_kos_kontrakan3` text DEFAULT NULL,
  `jenis_hunian` varchar(50) DEFAULT NULL,
  `jenis_penghuni` varchar(20) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `ukuran_kamar` varchar(20) DEFAULT NULL,
  `jumlah_kamar` int(10) DEFAULT NULL,
  `jenis_sewa` varchar(20) DEFAULT NULL,
  `jenis_fasilitas` text DEFAULT NULL,
  `tgl_upload` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kos_kontrakan`
--

INSERT INTO `tb_kos_kontrakan` (`id_kos_kontrakan`, `id_pemilik`, `nama_kos_kontrakan`, `alamat_kos_kontrakan`, `no_telpon_kos_kontrakan`, `ket_kos_kontrakan`, `gambar_kos_kontrakan1`, `gambar_kos_kontrakan2`, `gambar_kos_kontrakan3`, `jenis_hunian`, `jenis_penghuni`, `harga`, `ukuran_kamar`, `jumlah_kamar`, `jenis_sewa`, `jenis_fasilitas`, `tgl_upload`) VALUES
('GTC20201946KN', '1738556ZLP83O2020', 'RIYAS HUNIAN', 'Padang', '8978978', 'Padang', 'f1KZO-1942-images (2).jpg', 'f2KZO-1942-images (5).jpg', 'f3KZO-1942-images.jpg', 'Kos-kosan', 'Putra', 30000, '2X4', 10, 'Bulan', 'Kamar Mandi,Kasur,Akses Kunci 24 Jam', '2020-04-03'),
('O6720201214KN', '1738556ZLP83O2020', 'Kontrakan keluarga', 'padang', '9723982732873', 'sakdskdsakjdshd', 'f1NH7-1227-images (1).jpg', 'f2NH7-1227-images (3).jpg', 'f3NH7-1227-images (5).jpg', 'Kontrakan', 'Campur', 2990000, '2x2', 0, 'Tahun', 'Kamar Mandi,Wifi Internet', '2020-04-10'),
('PO820201355KN', '133845O2NYZBH2020', 'MIfta HUnian', 'Padang utara', '123456789', 'Hanya untuk orang normal', 'f1C47-1359-Untitled.jpg', 'f2C47-1359-1.png', 'f3C47-1359-2.png ', 'Kos-kosan', 'Putri', 2990000, '2x2', 10, 'Bulan', 'Kamar Mandi,Kasur,AC,Wifi Internet,Akses Kunci 24 Jam', '2020-04-12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pemilik_kos_kontrakan`
--

CREATE TABLE `tb_pemilik_kos_kontrakan` (
  `id_pemilik` varchar(255) NOT NULL,
  `no_ktp_pemilik` varchar(255) DEFAULT NULL,
  `nama_pemilik` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(15) DEFAULT NULL,
  `no_telpon` varchar(50) DEFAULT NULL,
  `alamat_pemilik` text DEFAULT NULL,
  `foto_pemilik` text DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `no_whatsapp` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pemilik_kos_kontrakan`
--

INSERT INTO `tb_pemilik_kos_kontrakan` (`id_pemilik`, `no_ktp_pemilik`, `nama_pemilik`, `jenis_kelamin`, `no_telpon`, `alamat_pemilik`, `foto_pemilik`, `email`, `no_whatsapp`) VALUES
('133845O2NYZBH2020', '12345', 'MIfta', 'Perempuan', '13456789', 'Pasisia', 'U85PYKM-133920-1.png', '1@gmail.com', '6234567890'),
('1738556ZLP83O2020', '353534543545', 'Riyas', 'Laki-laki', '32454354', 'Padang pajang', 'D8Y32RK-174020-ADM.png', 'riyas@gmail.com', '6282394740185'),
('192138UZ1X3QR2020', '345675432456', 'edfgedfsfd', 'Laki-laki', '1', 'ddadasds', 'FXQGHMP-192200-2.png', '1@gmail.com', '621');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(50) NOT NULL,
  `nama_user` varchar(100) DEFAULT NULL,
  `email_user` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `id_pemilik` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama_user`, `email_user`, `username`, `password`, `status`, `id_pemilik`) VALUES
(1, 'admin', 'riyas@gmail.com', 'admin', '12345', 'admin', '-'),
(2, 'Mifta', 'mifta@gmail.com', 'mifta', '12345', 'Pemilik', '133845O2NYZBH2020'),
(3, 'Riyas', 'riyas@gmail.com', 'riyas', '12345', 'Pemilik', '1738556ZLP83O2020'),
(7, 'edfgedfsfd', '1@gmail.com', 'kacau', '12345', 'Pemilik', '192138UZ1X3QR2020');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_booking`
--
ALTER TABLE `tb_booking`
  ADD PRIMARY KEY (`no_booking`);

--
-- Indeks untuk tabel `tb_kos_kontrakan`
--
ALTER TABLE `tb_kos_kontrakan`
  ADD PRIMARY KEY (`id_kos_kontrakan`) USING BTREE;

--
-- Indeks untuk tabel `tb_pemilik_kos_kontrakan`
--
ALTER TABLE `tb_pemilik_kos_kontrakan`
  ADD PRIMARY KEY (`id_pemilik`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
