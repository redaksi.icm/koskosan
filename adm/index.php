<!DOCTYPE html>
<html lang="en" class="body-full-height">

<head>
    <!-- META SECTION -->
    <title>LOGIN-ADMINISTRATOR</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- <link rel="icon" href="favicon.ico" type="image/x-icon" /> -->
    <link rel="icon" type="image/png" href="../desain/logo/kos-kosan.png">
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="desain_adm/css/theme-default.css" />
    <!-- EOF CSS INCLUDE -->
</head>

<body>
    <div class="login-container" style="background: url(../desain/logo/kos-kosan.png) left top repeat #1b1e24; background-position: left;background-repeat: no-repeat;background-size: contain;">
        <div class="login-box animated fadeInDown">
            <div class="login-logo"></div>
            <div class="login-body" style="background: rgb(255, 255, 255);">
                <div class="login-title" style="color: black;"><strong>Login Administrator</strong></div>
                <form action="action/proses_login.php" class="form-horizontal" method="POST">
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control"  name="username" placeholder="Username" style="background: rgb(27, 30, 36);"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="password" class="form-control"  name="password" placeholder="Password" style="background: rgb(27, 30, 36);"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <select class="form-control select" name="status" style="background: rgb(27, 30, 36);">
                                <option value="0">Pilih Status</option>
                                <option value="Pemilik">Pemilik</option>
                                <option value="admin">Admin</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <a href="../index.php" class="btn btn-link btn-block" style="color: black;">Back to website!</a>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-info btn-block">Log In</button>
                        </div>
                    </div>
                    <div class="login-subtitle">
                        <!-- Belum Memiliki Akun? <a href="register.php">Create an account</a> -->
                    </div>
                </form>
            </div>
            <div class="login-footer" style="background: #33ca32;">
                <div class="pull-left">
                    Create by: 2020 KOS-KOSAN
                </div>
            </div>
        </div>

    </div>

</body>

</html>