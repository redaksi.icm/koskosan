<?php
$permitted_chars = '0123456789ABCDFGHOJKLMNOPQRSTUVWXYZ';
$kode_random = substr(str_shuffle($permitted_chars), 0, 7);
$tahun = date("Y");
$time = date("His");


if (isset($_POST['simpan'])) {
    $ukuran  = $_FILES['foto_pemilik']['size'];

    if ($ukuran == 0) {
        $nama = $_POST['foto_pemilik_edit'];
        $ekstensi_diperbolehkan = array('jpg', 'png', 'jpeg');
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
    } else {

        $ekstensi_diperbolehkan = array('jpg', 'png', 'jpeg');
        $nama_pemilik = $_FILES['foto_pemilik']['name'];
        $nama = $kode_random . '-' . $time . '-' . $nama_pemilik;
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran    = $_FILES['foto_pemilik']['size'];
        $file_tmp = $_FILES['foto_pemilik']['tmp_name'];
    }

    if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
        if ($ukuran == 0) {
        } else {
            move_uploaded_file($file_tmp, '../gambar_adm/' . $nama);
        }

        $updatekan = mysqli_query($connect, "UPDATE tb_pemilik_kos_kontrakan SET  
                                            no_ktp_pemilik='$_POST[no_ktp]', 
                                            nama_pemilik='$_POST[nama_pemilik]',
                                            jenis_kelamin='$_POST[kelamin]',
                                            no_telpon='$_POST[no_telpon]',
                                            alamat_pemilik='$_POST[alamat_pemilik]',
                                            foto_pemilik='$nama',
                                            email='$_POST[email_pemilik]',
                                            no_whatsapp='$_POST[no_whatsapp]'
                                            where id_pemilik ='$_GET[id]'");

        if (!$updatekan) {
            echo '<div class="row">
            <div class="col-md-12"><div class="alert alert-danger" role="alert">
                <a href="home_adm.php?administrator=6&id=' . $_GET["id"] . '"class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                    <strong>Gagal!</strong> Ada kesalahan, Coba cek data kembali.
                </div>
            </div>
        </div> ';
        } else {
            echo '<div class="row">
            <div class="col-md-12">
                <div class="alert alert-info" role="alert">
                <a href="home_adm.php?administrator=6&id=' . $_GET["id"] . '" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                <strong>Sukses!</strong> Data berhasil di update...
                </div>
            </div>
        </div>';
        }
    } else {
    }
}


$no = 1;
$tampilkan = mysqli_query($connect, "SELECT * FROM tb_pemilik_kos_kontrakan WHERE id_pemilik = '$_GET[id]'");
foreach ($tampilkan as $data) {
?>

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background: #5e92b9;">
                        <h3 class="panel-title">EDIT DATA PEMILIK</h3>
                    </div>
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">ID PEMILIK</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" name="id_pemilik" class="form-control" value="<?php echo $data['id_pemilik']; ?>" readonly style="background: #428bca3b; color: black;" />
                                            </div>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">NO KTP</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="number" class="form-control" name="no_ktp" value="<?php echo $data['no_ktp_pemilik']; ?>" />
                                            </div>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">NAMA PEMILIK</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="nama_pemilik" value="<?php echo $data['nama_pemilik']; ?>" />
                                            </div>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">JENIS KELAMIN</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    <label class="check"><input type="radio" class="iradio" name="kelamin" value="Laki-laki" <?php if ($data['jenis_kelamin'] == 'Laki-laki') {
                                                                                                                                                    echo 'checked="checked"';
                                                                                                                                                } else {
                                                                                                                                                    echo '';
                                                                                                                                                }; ?> /> Laki-laki</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="check"><input type="radio" class="iradio" name="kelamin" value="Perempuan" <?php if ($data['jenis_kelamin'] == 'Perempuan') {
                                                                                                                                                    echo 'checked="checked"';
                                                                                                                                                } else {
                                                                                                                                                    echo '';
                                                                                                                                                }; ?> /> Perempuan</label>
                                                </div>
                                            </div>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">EMAIL</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="email_pemilik" value="<?php echo $data['email']; ?>" />
                                            </div>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">NO TELPON</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="no_telpon" value="<?php echo $data['no_telpon']; ?>" />
                                            </div>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">NO WHATSAPP</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" value="<?php echo $data['no_whatsapp']; ?>" name="no_whatsapp" />
                                            </div>
                                            <span class="help-block">Nomor harus terdaftar di app whatsapp</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">ALAMAT PEMILIK</label>
                                        <div class="col-md-9 col-xs-12">
                                            <textarea class="form-control" rows="5" name="alamat_pemilik"><?php echo $data['alamat_pemilik']; ?></textarea>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">FOTO PEMILIK</label>
                                        <div class="col-md-9 col-xs-12">
                                            <input type="file" multiple id="file-simple" name="foto_pemilik" />
                                            <input type="hidden" name="foto_pemilik_edit" value="<?php echo $data['foto_pemilik']; ?>" />


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <!-- <a href="home_adm.php?administrator=3" class="btn btn-default">Kembali</a> -->
                            <button class="btn btn-primary pull-right" type="submit" name="simpan">Simpan Data</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

<?php } ?>