<?php
session_start();
include "../../config/koneksi.php";
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
$status = $_SESSION['status'];
$id_pemilik = $_SESSION['id_pemilik'];
$q = mysqli_query($connect, "SELECT * from tb_user where id_user ='$id_user' and status='$status'");
if (mysqli_num_rows($q) == 0) {
    header("location:../page/error.php");
} else {
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- META SECTION -->
    <title>Administrator</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="../desain_adm/css/theme-default.css" />
    <style type="text/css">
        .scroll1 {
            width: 100%;
            height: 100%;
            margin: 30px 30px 30px 0px;
            /* padding: 0px 20px 20px 0px; */
            overflow-y: auto;
            overflow-x: scroll;
        }
    </style>
    <!-- EOF CSS INCLUDE -->
</head>

<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">

        <!-- START PAGE SIDEBAR -->
        <?php include 'sidebar.php' ?>
        <!-- END PAGE SIDEBAR -->

        <!-- PAGE CONTENT -->
        <div class="page-content" style="background: url(../../desain/logo/logo2.webp)left top repeat #f5f5f5; background-position: center;background-repeat: no-repeat;background-size: contain;">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel" style="background: #009b4c">
                <!-- TOGGLE NAVIGATION -->
                <li class="xn-icon-button">
                    <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                </li>
                <!-- END TOGGLE NAVIGATION -->
                <!-- SEARCH -->
                <!-- END SEARCH -->
                <!-- SIGN OUT -->
                <li class="xn-icon-button pull-right">
                    <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>
                </li>
                <!-- END SIGN OUT -->
                <!-- MESSAGES -->
                <!-- <li class="xn-icon-button pull-right">
                    <a href="#"><span class="fa fa-comments"></span></a>
                    <div class="informer informer-danger">4</div>
                    <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="fa fa-comments"></span> Messages</h3>
                            <div class="pull-right">
                                <span class="label label-danger">4 new</span>
                            </div>
                        </div>
                        <div class="panel-body list-group list-group-contacts scroll" style="height: 200px;">
                            <a href="#" class="list-group-item">
                                <div class="list-group-status status-online"></div>
                                <img src="assets/images/users/user2.jpg" class="pull-left" alt="John Doe" />
                                <span class="contacts-title">John Doe</span>
                                <p>Praesent placerat tellus id augue condimentum</p>
                            </a>
                            <a href="#" class="list-group-item">
                                <div class="list-group-status status-away"></div>
                                <img src="assets/images/users/user.jpg" class="pull-left" alt="Dmitry Ivaniuk" />
                                <span class="contacts-title">Dmitry Ivaniuk</span>
                                <p>Donec risus sapien, sagittis et magna quis</p>
                            </a>
                            <a href="#" class="list-group-item">
                                <div class="list-group-status status-away"></div>
                                <img src="assets/images/users/user3.jpg" class="pull-left" alt="Nadia Ali" />
                                <span class="contacts-title">Nadia Ali</span>
                                <p>Mauris vel eros ut nunc rhoncus cursus sed</p>
                            </a>
                            <a href="#" class="list-group-item">
                                <div class="list-group-status status-offline"></div>
                                <img src="assets/images/users/user6.jpg" class="pull-left" alt="Darth Vader" />
                                <span class="contacts-title">Darth Vader</span>
                                <p>I want my money back!</p>
                            </a>
                        </div>
                        <div class="panel-footer text-center">
                            <a href="pages-messages.html">Show all messages</a>
                        </div>
                    </div>
                </li> -->
                <!-- END MESSAGES -->
                <!-- TASKS -->
                <!-- END TASKS -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->

            <!-- START BREADCRUMB -->
            <ul class="breadcrumb">
            <?php include 'url_page.php'?>
            </ul>
            <!-- END BREADCRUMB -->

            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                <div class="row">
                    <?php include '../action/loading_adm.php'; ?>
                </div>
                <div class="chart-holder" id="dashboard-area-1" style="height: 200px;"></div>
                <div class="block-full-width">

                </div>
                <!-- END DASHBOARD CHART -->

            </div>
            <!-- END PAGE CONTENT WRAPPER -->
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="../action/logout.php" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->

    <!-- START PRELOADS -->
    <audio id="audio-alert" src="../desain_adm/audio/alert.mp3" preload="auto"></audio>
    <audio id="audio-fail" src="../desain_adm/audio/fail.mp3" preload="auto"></audio>
    <script type="text/javascript" src="../desain_adm/js/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../desain_adm/js/plugins/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../desain_adm/js/plugins/bootstrap/bootstrap.min.js"></script>
    <script type='text/javascript' src='../desain_adm/js/plugins/icheck/icheck.min.js'></script>
    <script type="text/javascript" src="../desain_adm/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>

    <script type="text/javascript" src="../desain_adm/js/plugins/codemirror/codemirror.js"></script>
    <script type='text/javascript' src="../desain_adm/js/plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <script type='text/javascript' src="../desain_adm/js/plugins/codemirror/mode/xml/xml.js"></script>
    <script type='text/javascript' src="../desain_adm/js/plugins/codemirror/mode/javascript/javascript.js"></script>
    <script type='text/javascript' src="../desain_adm/js/plugins/codemirror/mode/css/css.js"></script>
    <script type='text/javascript' src="../desain_adm/js/plugins/codemirror/mode/clike/clike.js"></script>
    <script type='text/javascript' src="../desain_adm/js/plugins/codemirror/mode/php/php.js"></script>

    <script type="text/javascript" src="../desain_adm/js/plugins/summernote/summernote.js"></script>

    <script type="text/javascript" src="../desain_adm/js/plugins/datatables/jquery.dataTables.min.js"></script>

    <script type="text/javascript" src="../desain_adm/js/plugins/knob/jquery.knob.min.js"></script>

    <script type="text/javascript" src="../desain_adm/js/plugins/scrolltotop/scrolltopcontrol.js"></script>
    <script type="text/javascript" src="../desain_adm/js/plugins/morris/raphael-min.js"></script>
    <script type="text/javascript" src="../desain_adm/js/plugins/morris/morris.min.js"></script>
    <script type="text/javascript" src="../desain_adm/js/plugins/rickshaw/d3.v3.js"></script>
    <script type="text/javascript" src="../desain_adm/js/plugins/rickshaw/rickshaw.min.js"></script>
    <script type='text/javascript' src='../desain_adm/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
    <script type='text/javascript' src='../desain_adm/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>
    <script type='text/javascript' src='../desain_adm/js/plugins/bootstrap/bootstrap-datepicker.js'></script>
    <script type="text/javascript" src="../desain_adm/js/plugins/owl/owl.carousel.min.js"></script>
    <script type="text/javascript" src="../desain_adm/js/plugins/moment.min.js"></script>
    <script type="text/javascript" src="../desain_adm/js/plugins/daterangepicker/daterangepicker.js"></script>
    <script type='text/javascript' src='../desain_adm/js/plugins/noty/jquery.noty.js'></script>
    <script type='text/javascript' src='../desain_adm/js/plugins/noty/layouts/topCenter.js'></script>
    <script type='text/javascript' src='../desain_adm/js/plugins/noty/layouts/topLeft.js'></script>
    <script type='text/javascript' src='../desain_adm/js/plugins/noty/layouts/topRight.js'></script>
    <script type='text/javascript' src='../desain_adm/js/plugins/noty/themes/default.js'></script>
    <script type="text/javascript" src="../desain_adm/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="../desain_adm/js/plugins/bootstrap/bootstrap-select.js"></script>
    <script type="text/javascript" src="../desain_adm/js/plugins/bootstrap/bootstrap-file-input.js"></script>


    <script>
        var editor = CodeMirror.fromTextArea(document.getElementById("codeEditor"), {
            lineNumbers: true,
            matchBrackets: true,
            mode: "application/x-httpd-php",
            indentUnit: 4,
            indentWithTabs: true,
            enterMode: "keep",
            tabMode: "shift"
        });
        editor.setSize('100%', '420px');
    </script>




    <script type="text/javascript">
        function notyConfirm() {
            noty({
                text: 'Do you want to continue?',
                layout: 'topRight',
                buttons: [{
                        addClass: 'btn btn-success btn-clean',
                        text: 'Ok',
                        onClick: function($noty) {
                            $noty.close();
                            noty({
                                text: 'You clicked "Ok" button',
                                layout: 'topRight',
                                type: 'success'
                            });
                        }
                    },
                    {
                        addClass: 'btn btn-danger btn-clean',
                        text: 'Cancel',
                        onClick: function($noty) {
                            $noty.close();
                            noty({
                                text: 'You clicked "Cancel" button',
                                layout: 'topRight',
                                type: 'error'
                            });
                        }
                    }
                ]
            })
        }
    </script>
    <!-- END PAGE PLUGINS -->



    <!-- START TEMPLATE -->
    <script type="text/javascript" src="../desain_adm/js/plugins/dropzone/dropzone.min.js"></script>
    <script type="text/javascript" src="../desain_adm/js/plugins/fileinput/fileinput.min.js"></script>
    <script type="text/javascript" src="../desain_adm/js/plugins/filetree/jqueryFileTree.js"></script>

    <script type="text/javascript" src="../desain_adm/js/settings.js"></script>
    <script type="text/javascript" src="../desain_adm/js/plugins.js"></script>
    <script type="text/javascript" src="../desain_adm/js/actions.js"></script>
    <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->

    <script>
        $(function() {
            $("#file-simple").fileinput({
                showUpload: false,
                showCaption: false,
                browseClass: "btn btn-danger",
                fileType: "any"
            });
            $("#filetree").fileTree({
                root: '/',
                script: 'assets/filetree/jqueryFileTree.php',
                expandSpeed: 100,
                collapseSpeed: 100,
                multiFolder: false
            }, function(file) {
                alert(file);
            }, function(dir) {
                setTimeout(function() {
                    page_content_onresize();
                }, 200);
            });

            // batas

            $("#file-simple2").fileinput({
                showUpload: false,
                showCaption: false,
                browseClass: "btn btn-danger",
                fileType: "any"
            });

            // batas
            $("#file-simple3").fileinput({
                showUpload: false,
                showCaption: false,
                browseClass: "btn btn-danger",
                fileType: "any"
            });
        });
    </script>
</body>

</html>