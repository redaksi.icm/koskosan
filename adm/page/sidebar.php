<?php
include "../../config/koneksi.php";
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
$status = $_SESSION['status'];
$id_pemilik = $_SESSION['id_pemilik'];
if ($_SESSION['id_user'] == null || $_SESSION['id_user'] == 0) {
    // header("location:login/error.php");
} else {
}
?>

<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <li class="xn-logo">
            <a href="home_adm.php" style="background: #009b4c;">KOS-KOSAN</a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="../../desain/logo/InfoKost.png" alt="John Doe" />
            </a>
            <div class="profile">
                <div class="profile-image">
                    <img src="../../desain/logo/InfoKost.png" alt="John Doe" />
                </div>
                <div class="profile-data">
                    <div class="profile-data-name"><?php echo $nama_user; ?></div>
                    <div class="profile-data-title"><?php echo $status; ?></div>
                    <div class="profile-data-title"><?php echo $id_pemilik; ?></div>
                </div>
            </div>
        </li>
        <li class="xn-title">Navigation</li>
        <li <?php if(@$_GET['administrator'] == 1 || @$_GET['administrator'] == 0) {echo 'class="active"'; }else{echo'';} ?>>
            <a href="home_adm.php"><span class="fa fa-desktop"></span> <span class="xn-text">DASHBOARD</span></a>
        </li>
<!-- batas admin -->
        <?php if ($_SESSION['status'] == 'admin'){?>

        <li <?php if(@$_GET['administrator'] == 2 || @$_GET['administrator'] == 3 || @$_GET['administrator'] == 5 || @$_GET['administrator'] == 6 ) {echo 'class="xn-openable active"'; }else{echo'class="xn-openable"';} ?> >
            <a href="#"><span class="fa fa-group"></span> <span class="xn-text">DATA PEMILIK</span></a>
            <ul>
                <li <?php if(@$_GET['administrator'] == 2) {echo 'style="background-color: #ffffff;"'; }else{echo'';} ?> ><a href="home_adm.php?administrator=2">INPUT DATA</a></li>
                <li <?php if(@$_GET['administrator'] == 3 || @$_GET['administrator'] == 5 || @$_GET['administrator'] == 6) {echo 'style="background-color: #ffffff;"'; }else{echo'';} ?> ><a href="home_adm.php?administrator=3">TAMPILAN</a></li>
            </ul>
        </li>
        <li <?php if(@$_GET['administrator'] == 8 || @$_GET['administrator'] == 9 || @$_GET['administrator'] == 10 || @$_GET['administrator'] == 11 ||  @$_GET['administrator'] == 13) {echo 'class="xn-openable active"'; }else{echo'class="xn-openable"';} ?>>
            <a href="#"><span class="fa fa-home"></span><span class="xn-text">DATA KOS-KONTRAKAN</span></a>
            <ul>
                <li <?php if(@$_GET['administrator'] == 8) {echo 'style="background-color: #ffffff;"'; }else{echo'';} ?> ><a href="home_adm.php?administrator=8">INPUT DATA </a></li>
                <li <?php if(@$_GET['administrator'] == 9 || (@$_GET['administrator'] == 10 && @$_GET['j_link'] =='kos') || (@$_GET['administrator'] == 11 && @$_GET['j_link'] =='kos')) {echo 'style="background-color: #ffffff;"'; }else{echo'';} ?> ><a href="home_adm.php?administrator=9">TAMPILAN KOS</a></li>
                <li <?php if(@$_GET['administrator'] == 13 || (@$_GET['administrator'] == 10 && @$_GET['j_link'] =='kontrakan') || (@$_GET['administrator'] == 11 && @$_GET['j_link'] =='kontrakan')) {echo 'style="background-color: #ffffff;"'; }else{echo'';} ?> ><a href="home_adm.php?administrator=13">TAMPILAN KONTRAKAN</a></li>
            </ul>
        </li>
        <li <?php if(@$_GET['administrator'] == 18 ) {echo 'class="active"'; }else{echo'class=""';} ?>>
            <a href="home_adm.php?administrator=18"><span class="fa fa-folder"></span> <span class="xn-text">ARSIP DATA</span></a>
        </li>
        <li class="xn-title">SETTING</li>
        <li  <?php if(@$_GET['administrator'] == 19  || @$_GET['administrator'] == 20 || @$_GET['administrator'] == 21) {echo 'class="active"'; }else{echo'class=""';} ?>>
            <a href="home_adm.php?administrator=19"><span class="fa fa-male"></span> <span class="xn-text">KELOLA USER</span></a>
        </li>
        <li class="">
            <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-mail-reply"></span> <span class="xn-text">Keluar</span></a>
        </li>
        <?php }else{ ?>
<!-- batas pemilik -->
        <li <?php if(@$_GET['administrator'] == 8 || @$_GET['administrator'] == 9 || @$_GET['administrator'] == 10 || @$_GET['administrator'] == 11 ||  @$_GET['administrator'] == 13) {echo 'class="xn-openable active"'; }else{echo'class="xn-openable"';} ?>>
            <a href="#"><span class="fa fa-home"></span><span class="xn-text">DATA KOS-KONTRAKAN</span></a>
            <ul>
                <li <?php if(@$_GET['administrator'] == 9 || (@$_GET['administrator'] == 10 && @$_GET['j_link'] =='kos') || (@$_GET['administrator'] == 11 && @$_GET['j_link'] =='kos')) {echo 'style="background-color: #ffffff;"'; }else{echo'';} ?> ><a href="home_adm.php?administrator=9">TAMPILAN KOS</a></li>
                <li <?php if(@$_GET['administrator'] == 13 || (@$_GET['administrator'] == 10 && @$_GET['j_link'] =='kontrakan') || (@$_GET['administrator'] == 11 && @$_GET['j_link'] =='kontrakan')) {echo 'style="background-color: #ffffff;"'; }else{echo'';} ?> ><a href="home_adm.php?administrator=13">TAMPILAN KONTRAKAN</a></li>
            </ul>
        </li>
        <li <?php if(@$_GET['administrator'] == 18 ) {echo 'class="active"'; }else{echo'class=""';} ?>>
            <a href="home_adm.php?administrator=18"><span class="fa fa-folder"></span> <span class="xn-text">ARSIP DATA</span></a>
        </li>
        <li class="">
            <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-mail-reply"></span> <span class="xn-text">Keluar</span></a>
        </li>
        <?php } ?>
      
    </ul>
    <!-- END X-NAVIGATION -->
</div>


