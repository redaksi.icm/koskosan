<?php
$no = 1;
$tampilkan = mysqli_query($connect, "SELECT * FROM tb_kos_kontrakan inner join tb_pemilik_kos_kontrakan on tb_kos_kontrakan.id_pemilik = tb_pemilik_kos_kontrakan.id_pemilik where tb_kos_kontrakan.id_kos_kontrakan = '$_GET[id]'");
foreach ($tampilkan as $data) {


    $hobi = explode(",", $data['jenis_fasilitas']);
?>
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <!-- NEWS WIDGET -->
                <div class="panel panel-default">
                    <div class="panel-heading ui-draggable-handle">
                        <h3 class="panel-title">View data</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-6">
                            <div class="panel panel-colorful" style="background: #f5f5f5;">
                                <div class="widget widget-warning widget-carousel" style="background: white;">
                                    <div class="owl-carousel" id="owl-example">
                                        <div>
                                            <div class="panel-body panel-body-image">
                                                <img src="../gambar_adm/gambar_kos_kontrakan/<?php echo $data['gambar_kos_kontrakan1']; ?>" alt="Ocean" style="max-height: 300px" />
                                            </div>
                                        </div>
                                        <div>
                                            <div class="panel-body panel-body-image">
                                                <img src="../gambar_adm/gambar_kos_kontrakan/<?php echo $data['gambar_kos_kontrakan2']; ?>" alt="Ocean" style="max-height: 300px" />
                                            </div>
                                        </div>
                                        <div>
                                            <div class="panel-body panel-body-image">
                                                <img src="../gambar_adm/gambar_kos_kontrakan/<?php echo $data['gambar_kos_kontrakan3']; ?>" alt="Ocean" style="max-height: 300px"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel-body">
                                        <button class="btn btn-default" data-toggle="modal"><?php echo $data['jenis_penghuni']; ?></button>
                                        <button class="btn btn-warning" data-toggle="modal"><?php echo $data['jenis_hunian']; ?></button>
                                        <button class="btn btn-danger" data-toggle="modal">Per <?php echo $data['jenis_sewa']; ?></button>
                                        <p></p>
                                        <h3><?php echo $data['nama_kos_kontrakan']; ?></h3>
                                        <p><b>Luas Kamar</b></p>
                                        <div class="panel-body">
                                            <span><img src="../gambar_adm/logo_f/ic_room_size.svg" alt="" width="30px"></span>
                                            <span>
                                                <b style="font-size: 17px"><?php echo $data['ukuran_kamar']; ?></b>
                                            </span>
                                        </div>
                                        <div style="padding-bottom: 100px;"></div>
                                        <p><b style="font-size: 14px">Fasilitas dan kamar</b></p>
                                        <?php if (in_array('Wifi Internet', $hobi) ===  true) { ?>
                                            <div class="col-md-4">
                                                <p><img src="../gambar_adm/logo_f/wifi.png" alt="Ocean" style="height: 2.15em; margin-right: 1rem;" /><span>Wifi Internet</span></p>
                                            </div>
                                        <?php } ?>

                                        <?php if (in_array('Kamar Mandi', $hobi) ===  true) { ?>
                                            <div class="col-md-4">
                                                <p><img src="../gambar_adm/logo_f/kamar_mandi.png" alt="Ocean" style="height: 2.15em; margin-right: 1rem;" /><span>Kamar Mandi</span></p>
                                            </div>
                                        <?php } ?>

                                        <?php if (in_array('Kasur', $hobi) ===  true) { ?>
                                            <div class="col-md-4">
                                                <p><img src="../gambar_adm/logo_f/kasur.png" alt="Ocean" style="height: 2.15em; margin-right: 1rem;" /><span>Kasur</span></p>
                                            </div>
                                        <?php } ?>

                                        <?php if (in_array('AC', $hobi) ===  true) { ?>
                                            <div class="col-md-4">
                                                <p><img src="../gambar_adm/logo_f/AC.png" alt="Ocean" style="height: 2.15em; margin-right: 1rem;" /><span>AC</span></p>
                                            </div>
                                        <?php } ?>

                                        <?php if (in_array('Akses Kunci 24 Jam', $hobi) ===  true) { ?>
                                            <div class="col-md-5">
                                                <p><img src="../gambar_adm/logo_f/akses_kunci.png" alt="Ocean" style="height: 2.15em; margin-right: 1rem;" /><span>Akses Kunci 24 Jam</span></p>
                                            </div>
                                        <?php } ?>

                                        <div style="padding-bottom: 80px;"></div>
                                        <div class="col-md-12">
                                            <p><b style="font-size: 14px">Deskripsi</b></p>
                                            <p><?php echo $data['ket_kos_kontrakan']; ?></p>
                                            <p><b style="font-size: 14px">Alamat</b></p>
                                            <p><?php echo $data['alamat_kos_kontrakan']; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title">Detail</h3>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>No Telpon</th>
                                                <th>Harga</th>
                                                <th>Jumlah kamar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?php echo $data['id_kos_kontrakan'];?></td>
                                                <td><?php echo $data['no_telpon_kos_kontrakan'];?></td>
                                                <td><?php echo $data['harga'];?></td>
                                                <td><?php echo $data['jumlah_kamar'];?></td>
                                            </tr>                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>