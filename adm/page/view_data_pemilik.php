<?php
$no = 1;
$tampilkan = mysqli_query($connect, "SELECT * FROM tb_pemilik_kos_kontrakan WHERE id_pemilik = '$_GET[id]'");
foreach ($tampilkan as $data) {
?>
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background: #5e92b9;">
                        <h3 class="panel-title">VIEW DATA PEMILIK</h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                            <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                        </ul>
                    </div>
                    <br><br>
                    <br><br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="panel panel-default" style="width: 270px;">
                                    <div class="panel-body profile" style="height: 250px; background-color: #5e92b9;">
                                        <div class="panel-body panel-body-image">
                                            <img src="../gambar_adm/<?php echo $data['foto_pemilik']; ?>" alt="John Doe" style="width: inherit;height: -webkit-fill-available;">
                                        </div>
                                    </div>
                                    <div class="panel-body list-group">
                                        <br>
                                        <a target="_blank" href="https://wa.me/<?php echo $data['no_whatsapp'];?>?text=Halo bapak/Ibuk, salam kenal, maaf menggangu kami dari kos-kosan" class="btn btn-info btn-block"><span class="fa fa-comments-o"></span>CHAT VIA WHATSAPP</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="panel-body">
                                    <div class="panel-heading" style="background: #5e92b9;">
                                        <h4>DETAIL</h4>
                                    </div>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>:</th>
                                                <th><?php echo $data['id_pemilik']; ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="active">
                                                <td>NAMA</td>
                                                <td>:</td>
                                                <td><?php echo $data['nama_pemilik']; ?></td>
                                            </tr>
                                            <tr class="active">
                                                <td>KELAMIN</td>
                                                <td>:</td>
                                                <td><?php echo $data['jenis_kelamin']; ?></td>
                                            </tr>
                                            <tr class="active">
                                                <td>NO KTP</td>
                                                <td>:</td>
                                                <td><?php echo $data['no_ktp_pemilik']; ?></td>
                                            </tr>
                                            <tr class="success">
                                                <td>NO TELFON</td>
                                                <td>:</td>
                                                <td><?php echo $data['no_telpon']; ?></td>
                                            </tr>
                                            <tr class="success">
                                                <td>NO HWATSAPP</td>
                                                <td>:</td>
                                                <td><?php echo $data['no_whatsapp']; ?></td>
                                            </tr>
                                            <tr class="info">
                                                <td>EMAIL</td>
                                                <td>:</td>
                                                <td><?php echo $data['email']; ?></td>
                                            </tr>
                                            <tr class="warning">
                                                <td>ALAMAT</td>
                                                <td>:</td>
                                                <td><?php echo $data['alamat_pemilik']; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>