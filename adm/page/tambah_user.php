<?php

if (isset($_POST['pilih_data'])) {

    $id_pemilik = $_POST['pilih_data'];
    $tampilkan = mysqli_query($connect, "SELECT * FROM tb_pemilik_kos_kontrakan  where id_pemilik='$id_pemilik'");
    foreach ($tampilkan as $data) {

        $id_pemilik = $data['id_pemilik'];
        $email = $data['email'];
        $nama_pemilik = $data['nama_pemilik'];
    }
} else {
}
?>
<?php
if (isset($_POST['simpan'])) {

    $id_pemilik = $_POST['pilih_data'];
    $nama_user = $_POST['nama_pemilik'];
    $email = $_POST['email'];
    $username = $_POST['username'];
    $password = $_POST['password'];

    $simpan = mysqli_query($connect, "INSERT INTO tb_user (nama_user,email_user,username,password,status,id_pemilik) values ('$nama_user','$email','$username','$password','Pemilik','$id_pemilik')");


    if (!$simpan) {
        echo '
        <script type="text/javascript">
            window.setTimeout(function(){ 
                window.location.replace("home_adm.php?administrator=20&sukses=Berhasil");
            } ,2000); 
        </script>
        <div class="row">
                <div class="col-md-12"><div class="alert alert-danger" role="alert">
                    <a href="home_adm.php?administrator=20&sukses=Berhasil"class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                        <strong>Gagal!</strong> Ada kesalahan, Coba cek data kembali.
                    </div>
                </div>
    </div> ';
    } else {
        echo '
        <script type="text/javascript">
            window.setTimeout(function(){ 
                window.location.replace("home_adm.php?administrator=20&sukses=Berhasil");
            } ,2000); 
        </script>            
        <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info" role="alert">
                    <a href="home_adm.php?administrator=20&sukses=404" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                    <strong>Sukses!</strong> Data berhasil di simpan...
                    </div>
                </div>
            </div>';
    }
} else {
}


?>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="background: #5e92b9;">
                    <h3 class="panel-title">INPUT DATA USER</h3>
                </div>
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">CARI ID PEMILIK</label>
                                    <div class="col-md-9 col-xs-12">
                                        <select class="form-control select" data-live-search="true" name="pilih_data" onchange="this.form.submit()">
                                            <option value="0">PILIH ID PEMILIK</option>
                                            <?php
                                            include "../../config/koneksi.php";
                                            $tampilkan = mysqli_query($connect, "SELECT * FROM tb_pemilik_kos_kontrakan where id_pemilik NOT IN (SELECT id_pemilik FROM tb_user)");
                                            foreach ($tampilkan as $data) {
                                            ?>
                                                <option <?php if ($data['id_pemilik'] == @$id_pemilik) {
                                                            echo 'selected';
                                                        } ?> value="<?php echo $data['id_pemilik']; ?>"><?php echo $data['id_pemilik']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <span class="help-block" style="padding-top: 40px;"></span>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">EMAIL</label>
                                    <div class="col-md-9 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" class="form-control" name="email" value="<?php echo @$email; ?>" readonly style="background: #428bca3b; color: black;" />
                                        </div>
                                        <span class="help-block" style="padding-top: 20px;"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">NAMA USER</label>
                                    <div class="col-md-9 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" class="form-control" name="nama_pemilik" value="<?php echo @$nama_pemilik; ?>" readonly style="background: #428bca3b; color: black;" />
                                        </div>
                                        <span class="help-block" style="padding-top: 20px;"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-warning"></div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">USERNAME</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" class="form-control" name="username" />
                                    </div>
                                    <span class="help-block" style="padding-top: 20px;"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">PASSWORD</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" class="form-control" name="password" />
                                    </div>
                                    <span class="help-block" style="padding-top: 20px;"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <i>*.ID PEMILIK tidak tampil lagi apa bila sudah di buat akun user, untuk akses loginnya.</i>
                    
            </div>
            <div class="panel-footer">
                <!-- <button class="btn btn-default">Kembali</button> -->
                <button class="btn btn-primary pull-right" type="submit" name="simpan">Simpan Data</button>
            </div>
            </form>
        </div>
    </div>
</div>

                                                    </div>