<div class="page-content-wrap">
    <div class="row">
        <?php if (@$_GET['id'] == 'sukses') {
            echo '
                <script type="text/javascript">
                    window.setTimeout(function(){ 
                        window.location.replace("home_adm.php?administrator=19");
                    } ,2000); 
                </script>            
                <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert">
                                <a href="home_adm.php?administrator=19" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                                <strong>Sukses!</strong> Data berhasil di hapus...
                            </div>
                        </div>
                    </div>';
        }
        ?>
        <div class="col-md-12">
            <div class="panel panel-default">
                <a href="home_adm.php?administrator=20" class="btn btn-warning">TAMBAH DATA</a>
                <br><br>
                <div class="panel-heading" style="background: #5e92b9;">
                    <h3 class="panel-title">TAMPILAN DATA USER</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>ID USER</th>
                                <th>NAMA USER</th>
                                <th>EMAIL USER</th>
                                <th>STATUS USER</th>
                                <th>USERNAME</th>
                                <th>PASSWORD</th>
                                <th>ID PEMILIK</th>
                                <th style="align-items: center">ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampilkan = mysqli_query($connect, "SELECT * FROM tb_user where status='Pemilik'  ORDER BY id_user DESC");
                            foreach ($tampilkan as $data) {

                            ?>
                                <tr>
                                    <td><?php echo $data['id_user']; ?></td>
                                    <td><?php echo $data['nama_user']; ?></td>
                                    <td><?php echo $data['email_user']; ?></td>
                                    <td><?php echo $data['status']; ?></td>
                                    <td><?php echo $data['username']; ?></td>
                                    <td><?php echo $data['password']; ?></td>
                                    <td><?php echo $data['id_pemilik']; ?></td>
                                    <td>
                                        <ul class="panel-controls pull-left" style="margin-top: 2px;">
                                            <li><a href="home_adm.php?administrator=21&id=<?php echo $data['id_user']; ?>"><span class="fa fa-pencil"></span></a></li>
                                            <li><a href="home_adm.php?administrator=22&id=<?php echo $data['id_user']; ?>"><span class="fa fa-trash-o"></span></a></li>
                                        </ul>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>