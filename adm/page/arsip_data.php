<?php
include "../../config/koneksi.php";
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
$id_pemilik = $_SESSION['id_pemilik'];
$status = $_SESSION['status'];
$q = mysqli_query($connect, "SELECT * from tb_user where id_user ='$id_user' and status='$status'");
if (mysqli_num_rows($q) == 0) {
    header("location:login/error.php");
} else {
}
?>

<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <?php if (@$_GET['Berhasil'] == 'delete') {
                    echo '
                            
                                <div class="col-md-12">
                                    <div class="alert alert-info push-down-20">
                                        <span style="color: #FFF500;">Delete!</span> Data berhasil di hapus!!.
                                        <a href="home_adm.php?administrator=18" type="" class="close" data-dismiss="alert">×</a>
                                    </div>                                
                             </div>';
                } else {
                    echo '';
                }
                ?>
                <div class="panel-heading" style="background: #5e92b9;">
                    <h3 class="panel-title">ARSIP DATA</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID BOOKING</th>
                                <th>NAMA PEMBOOKING</th>
                                <th>JENIS KELAMIN</th>
                                <th style="align-items: center">ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            if ($_SESSION['status'] == 'admin') {
                            $tampilkan = mysqli_query($connect, "SELECT * FROM tb_booking inner join tb_kos_kontrakan on tb_booking.id_kos_kontrakan=tb_kos_kontrakan.id_kos_kontrakan where tb_booking.status_booking='Selesai'");
                            }else{
                                $tampilkan = mysqli_query($connect, "SELECT * FROM tb_booking inner join tb_kos_kontrakan on tb_booking.id_kos_kontrakan=tb_kos_kontrakan.id_kos_kontrakan where tb_booking.status_booking='Selesai' and tb_kos_kontrakan.id_pemilik='$id_pemilik'");

                            }
                            foreach ($tampilkan as $data) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data['no_booking']; ?></td>
                                    <td><?php echo $data['nama_sesuai_ktp']; ?></td>
                                    <td><?php echo $data['jenis_kelamin']; ?></td>
                                    <td>
                                        <ul class="panel-controls pull-left" style="margin-top: 2px;">
                                            <li title="View Invoice"><a href="home_adm.php?administrator=14&kategori=1&cari=<?php echo $data['no_booking']; ?>"><span class="fa fa-eye"></span></a></li>
                                            <?php if ($_SESSION['status'] == 'admin') { ?>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a onClick="return confirm('Apakah anda yakin, menghapus transaksi ini??')" href="home_adm.php?administrator=17&kategori=1&id=<?php echo $data['no_booking']; ?>" class=""><span class="fa fa-times"></span> Delete</a></li>
                                                    </ul>
                                                </li>
                                            <?php } else { ?> <?php } ?>
                                        </ul>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>