<?php
include "../../config/koneksi.php";
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
$status = $_SESSION['status'];
$id_pemilik = $_SESSION['id_pemilik'];
$q = mysqli_query($connect, "SELECT * from tb_user where id_user ='$id_user' and status='$status'");
if (mysqli_num_rows($q) == 0) {
    header("location:login/error.php");
} else {
}
?>
<div class="row">
    <div class="col-md-12">
        <!-- START PANEL WITH STATIC CONTROLS -->

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Panel Booking</h3>
                <ul class="panel-controls">
                    <!-- <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li> -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                            <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                        </ul>
                    </li>
                    <!-- <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li> -->
                </ul>
            </div>
            <?php if (@$_GET['Berhasil'] == 'berhasil') {
                echo '
            <div class="panel-body">
               <h5 style="color: red;">Data berhasil di Setujui, dan transaksi telah di selesaikan, silahkan lihat pada arsip data. <a href="home_adm.php?administrator=18">LINK</a></h5>
            </div>';
            } elseif (@$_GET['Berhasil'] == 'dibatalkan') {
                echo '
                <div class="panel-body">
                   <h3 style="color: red;">Transaksi telah di batalkan dan silahkan hapus..</h3>
                </div>';
            } elseif (@$_GET['Berhasil'] == 'delete') {
                echo '
                <div class="panel-body">
                   <h3 style="color: red;">Data berhasil di delete..</h3>
                </div>';
            } else {
                echo '';
            } ?>
            <div class="panel-body" style="background: #e8e8e8;">
                <div class="row">
                    <?php

                    if ($status == 'Pemilik') {
                     
                        $tampilkan = mysqli_query($connect, "SELECT * FROM tb_booking inner join tb_kos_kontrakan on tb_booking.id_kos_kontrakan=tb_kos_kontrakan.id_kos_kontrakan where (tb_booking.status_booking='Booking' or tb_booking.status_booking='Batal') and tb_kos_kontrakan.id_pemilik ='$id_pemilik'");

                    } else {

                        $tampilkan = mysqli_query($connect, "SELECT * FROM tb_booking inner join tb_kos_kontrakan on tb_booking.id_kos_kontrakan=tb_kos_kontrakan.id_kos_kontrakan where tb_booking.status_booking='Booking' or tb_booking.status_booking='Batal'");

                    }
                    $jumlah = mysqli_num_rows($tampilkan);

                    if ($jumlah == 0) {
                        echo '  <section id="contact" class="contact section" style="padding-top: 60px;padding-bottom: 60px;">
                                    <div class="container">
                                        <div class="col-lg-12 col-md-12 col-12">    
                                        <center><h1>Data Kosong.</h1></center>
                                            <img src="../../gambar/search.webp" style="display: block; margin: auto;height: -webkit-fill-available;">    
                                        </div>
                                    </div>
                                </section>';
                    } else {
                        echo '';
                    }

                    foreach ($tampilkan as $data) {

                    ?>
                        <div class="col-md-3">
                            <!-- CONTACT ITEM -->
                            <div class="panel panel-default">
                                <div class="panel-body profile" <?php if ($data['jenis_hunian'] == 'Kontrakan') {
                                                                    echo 'style="background:#009b53"';
                                                                } else {
                                                                    echo '';
                                                                } ?>>
                                    <div class="profile-image">
                                        <a href="#" class="tile" style="float: none;"><span class="fa fa-home"></span></a>
                                    </div>
                                    <div class="profile-data">
                                        <div class="profile-data-title" style="color: #fff;"><?php echo $data['nama_sesuai_ktp']; ?></div>
                                        <div class="profile-data-title" style="color: #fff;">Rp. <?php echo number_format($data['harga'], 0, ',', '.'); ?></div>
                                        <div class="profile-data-title" style="color: #fff;">TGL. <?php echo date('d-m-Y', strtotime($data['tgl_booking'])) ?> </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="contact-info">
                                        <p><small>ID Booking</small><br /><?php echo $data['no_booking']; ?></p>
                                        <?php if ($data['jenis_hunian'] == 'Kontrakan') { ?>
                                            <p><small>ID Kontrakan</small><br /><?php echo $data['id_kos_kontrakan']; ?></p>
                                        <?php } else { ?>
                                            <p><small>ID Kos</small><br /><?php echo $data['id_kos_kontrakan']; ?></p>
                                        <?php } ?>

                                        <p><small>Jenis Hunian</small><br /><?php echo $data['jenis_hunian']; ?></p>
                                        <p><small>No Telpon/ WA</small><br /><?php echo $data['no_telpon']; ?>/ <?php echo $data['no_whatsapp']; ?></p>
                                        <p><small>Batas Bayar</small><br /><?php echo date('d-m-Y h:i:s', strtotime('+3 days', strtotime($data['tgl_booking']))) ?></p>
                                        <p style="color: red"><small>Status Booking</small><br /><?php echo $data['status_booking']; ?></p>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="col-md-12" style="padding-right: 0px;padding-left: 0;">
                                        <?php if ($data['status_booking'] == 'Batal') { ?>
                                            <a onClick="return confirm('Apakah anda yakin, menghapus transaksi ini??')" href="home_adm.php?administrator=17&id=<?php echo $data['no_booking']; ?>" class="btn btn-info btn-block">DELETE</a>
                                        <?php } else { ?>
                                            <?php if ($data['jenis_hunian'] == 'Kontrakan') { ?>
                                                <a href="home_adm.php?administrator=10&id=<?php echo $data['id_kos_kontrakan']; ?>" class="btn btn-info" style="padding: 4px 10px;" title="View Data Kontrakan"><i class="glyphicon glyphicon-eye-open"></i></a>
                                            <?php } else { ?>
                                                <a href="home_adm.php?administrator=10&id=<?php echo $data['id_kos_kontrakan']; ?>" class="btn btn-info" style="padding: 4px 10px;" title="View Data Kos"><i class="glyphicon glyphicon-eye-open"></i></a>
                                            <?php } ?>
                                            <a target="_blank" href="https://wa.me/<?php echo $data['no_whatsapp']; ?>?text=Halo, Kami dari pihak pemilik kos/kontrakan. anda telah melakukan booking dengan no. booking <?php echo $data['no_booking']; ?>" class="btn btn-warning" style="padding: 4px 10px;" title="Hubungi Calon Penghuni"><i class="fa fa-comments-o"></i></a>

                                            <a href="home_adm.php?administrator=14&cari=<?php echo $data['no_booking']; ?>" class="btn btn-success" style="padding: 4px 10px;" title="View INVOICE"><i class="fa fa-credit-card"></i></a>
                                            <a onClick="return confirm('Apakah anda yakin, transaksi ini sudah selesai??')" href="home_adm.php?administrator=15&id=<?php echo $data['no_booking']; ?>" class="btn btn-primary" style="padding: 4px 10px;" title="Selesai"><i class="fa fa-check"></i></a>
                                            <a onClick="return confirm('Apakah anda yakin, membatalkan transaksi ini??')" href="home_adm.php?administrator=16&id=<?php echo $data['no_booking']; ?>" class="btn btn-danger" style="padding: 4px 10px;" title="Batal"><i class="fa fa-times"></i></a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <!-- END CONTACT ITEM -->
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="panel-footer">
                <!-- <button class="btn btn-primary pull-right">Button</button> -->
            </div>
        </div>
    </div>
</div>