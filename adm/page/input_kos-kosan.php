<?php
include "../../config/koneksi.php";
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
$status = $_SESSION['status'];
$q = mysqli_query($connect, "SELECT * from tb_user where id_user ='$id_user' and status='$status'");
if (mysqli_num_rows($q) == 0) {
    header("location:login/error.php");
} else {
}
?>

<?php

$permitted_chars = '0123456789ABCDFGHOJKLMNOPQRSTUVWXYZ';
$kode_random = substr(str_shuffle($permitted_chars), 0, 3);
$tahun = date("Y");
$tgl_upload = date("Y-m-d");
$time = date("Hs");


if (isset($_POST['pilih_data'])) {

    $id_pemilik = $_POST['pilih_data'];
    $tampilkan = mysqli_query($connect, "SELECT * FROM tb_pemilik_kos_kontrakan  where id_pemilik='$id_pemilik'");
    foreach ($tampilkan as $data) {

        $id_pemilik = $data['id_pemilik'];
        $ktp_pemilik = $data['no_ktp_pemilik'];
        $nama_pemilik = $data['nama_pemilik'];
    }
} else {
}


if (isset($_POST['simpan'])) {

    $ekstensi_diperbolehkan = array('jpg', 'png', 'jpeg');
    $foto_kos_1 = $_FILES['foto_kos_1']['name'];
    $nama_foto_kos_1 = 'f1' . $kode_random . '-' . $time . '-' . $foto_kos_1;
    $x_foto_kos_1 = explode('.', $nama_foto_kos_1);
    $ekstensi_foto_kos_1 = strtolower(end($x_foto_kos_1));
    $ukuran_foto_kos_1   = $_FILES['foto_kos_1']['size'];
    $file_tmp_foto_kos_1 = $_FILES['foto_kos_1']['tmp_name'];

    $ekstensi_diperbolehkan1 = array('jpg', 'png', 'jpeg');
    $foto_kos_2 = $_FILES['foto_kos_2']['name'];
    $nama_foto_kos_2 = 'f2' . $kode_random . '-' . $time . '-' . $foto_kos_2;
    $x_foto_kos_2 = explode('.', $nama_foto_kos_2);
    $ekstensi_foto_kos_2 = strtolower(end($x_foto_kos_2));
    $ukuran_foto_kos_2   = $_FILES['foto_kos_2']['size'];
    $file_tmp_foto_kos_2 = $_FILES['foto_kos_2']['tmp_name'];

    $ekstensi_diperbolehkan1 = array('jpg', 'png', 'jpeg');
    $foto_kos_3 = $_FILES['foto_kos_3']['name'];
    $nama_foto_kos_3 = 'f3' . $kode_random . '-' . $time . '-' . $foto_kos_3;
    $x_foto_kos_3 = explode('.', $nama_foto_kos_3);
    $ekstensi_foto_kos_3 = strtolower(end($x_foto_kos_3));
    $ukuran_foto_kos_3   = $_FILES['foto_kos_3']['size'];
    $file_tmp_foto_kos_3 = $_FILES['foto_kos_3']['tmp_name'];

    $fasilitas = implode(",", $_POST['fasilitas']);

    if (in_array($ekstensi_foto_kos_1, $ekstensi_diperbolehkan) === true) {

        move_uploaded_file($file_tmp_foto_kos_1, '../gambar_adm/gambar_kos_kontrakan/' . $nama_foto_kos_1);
        move_uploaded_file($file_tmp_foto_kos_2, '../gambar_adm/gambar_kos_kontrakan/' . $nama_foto_kos_2);
        move_uploaded_file($file_tmp_foto_kos_3, '../gambar_adm/gambar_kos_kontrakan/' . $nama_foto_kos_3);

        $simpan = mysqli_query($connect, "INSERT INTO tb_kos_kontrakan (id_kos_kontrakan, id_pemilik, nama_kos_kontrakan, alamat_kos_kontrakan, no_telpon_kos_kontrakan, ket_kos_kontrakan, gambar_kos_kontrakan1, gambar_kos_kontrakan2, gambar_kos_kontrakan3, jenis_hunian, jenis_penghuni, harga, ukuran_kamar, jumlah_kamar, jenis_sewa, jenis_fasilitas, tgl_upload) 
        values ('$_POST[id_kos_kosan]','$_POST[pilih_data]','$_POST[nama_kos_kontrakan]','$_POST[alamat_kosan]','$_POST[no_telpon_kosan]','$_POST[ket_kosan]','$nama_foto_kos_1','$nama_foto_kos_2','$nama_foto_kos_3','$_POST[jenis_hunian]','$_POST[jenis_penghuni]', '$_POST[harga_kos]', '$_POST[ukuran_kamar]','$_POST[jumlah_kamar]', '$_POST[jenis_sewa]', '$fasilitas', '$tgl_upload')");

        if (!$simpan) {
            echo '
            <script type="text/javascript">
                window.setTimeout(function(){ 
                    window.location.replace("home_adm.php?administrator=8&sukses=404");
                } ,2000); 
            </script>
            <div class="row">
                    <div class="col-md-12"><div class="alert alert-danger" role="alert">
                        <a href="home_adm.php?administrator=8&sukses=404"class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                            <strong>Gagal!</strong> Ada kesalahan, Coba cek data kembali.
                        </div>
                    </div>
        </div> ';
        } else {
            echo '
            <script type="text/javascript">
                window.setTimeout(function(){ 
                    window.location.replace("home_adm.php?administrator=8&sukses=404");
                } ,2000); 
            </script>            
            <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-info" role="alert">
                        <a href="home_adm.php?administrator=8&sukses=404" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                        <strong>Sukses!</strong> Data berhasil di simpan...
                        </div>
                    </div>
                </div>';
        }
    } else {
        echo '
        <script type="text/javascript">
                window.setTimeout(function(){ 
                    window.location.replace("home_adm.php?administrator=8&sukses=404");
                } ,2000); 
            </script>
        <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger" role="alert">
                        <a href="home_adm.php?administrator=8&sukses=404" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                        <strong>Gagal!</strong> type gambar harus jpg, png, jpeg.
                    </div>
                </div>
            </div>';
    }
} else {
}



?>



<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="background: #5e92b9;">
                    <h3 class="panel-title">INPUT DATA KOS-KOSAN/ KONTRAKAN</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>

                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">CARI ID PEMILIK</label>
                                        <div class="col-md-9 col-xs-12">
                                            <select class="form-control select" data-live-search="true" name="pilih_data" onchange="this.form.submit()">
                                                <option value="0">PILIH ID PEMILIK</option>
                                                <?php
                                                $tampilkan = mysqli_query($connect, "SELECT * FROM tb_pemilik_kos_kontrakan  ORDER BY id_pemilik DESC");
                                                foreach ($tampilkan as $data) {
                                                ?>
                                                    <option <?php if ($data['id_pemilik'] == @$id_pemilik) {
                                                                echo 'selected';
                                                            } ?> value="<?php echo $data['id_pemilik']; ?>"><?php echo $data['id_pemilik']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <span class="help-block" style="padding-top: 40px;"></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">NO KTP</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="number" class="form-control" name="no_ktp" value="<?php echo $ktp_pemilik; ?>" readonly style="background: #428bca3b; color: black;" />
                                            </div>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">NAMA PEMILIK</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="nama_pemilik" value="<?php echo @$nama_pemilik; ?>" readonly style="background: #428bca3b; color: black;" />
                                            </div>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-warning"></div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">ID KOS/KONTRAKAN</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="id_kos_kosan" value="<?php echo $kode_random . $tahun . $time . 'KN'; ?>" style="background: #428bca3b; color: black;" readonly />
                                            </div>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">JENIS HUNIAN</label>
                                        <div class="col-md-9">
                                            <select class="form-control select" data-live-search="true" name="jenis_hunian">
                                                <option value="Kos-kosan">Kos-kosan</option>
                                                <option value="Kontrakan">Kontrakan</option>
                                            </select>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">JENIS PENGHUNI</label>
                                        <div class="col-md-9">
                                            <select class="form-control select" data-live-search="true" name="jenis_penghuni">
                                                <option value="Putri">Putri</option>
                                                <option value="Putra">Putra</option>
                                                <option value="Campur">Campur</option>
                                            </select>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">JENIS SEWA</label>
                                        <div class="col-md-9">
                                            <select class="form-control select" data-live-search="true" name="jenis_sewa">
                                                <option value="Bulan">Perbulan</option>
                                                <option value="Tahun">Pertahun</option>
                                            </select>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">NAMA KOS/KONTRAKAN</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="nama_kos_kontrakan" />
                                            </div>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-5 control-label">ALAMAT KOS/KONTRAKAN</label>
                                        <div class="col-md-9 col-xs-12">
                                            <textarea class="form-control" rows="5" name="alamat_kosan"></textarea>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">NO TELPON</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="no_telpon_kosan" />
                                            </div>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-5 control-label">KETERANGAN</label>
                                        <div class="col-md-9 col-xs-12">
                                            <textarea class="form-control" rows="5" name="ket_kosan"></textarea>
                                            <span class="help-block" style="padding-top: 20px;"></span>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-5 control-label">FASILITAS</label>
                                        <div class="col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    <label class="check"><input type="checkbox" class="icheckbox" name="fasilitas[]" value="Kamar Mandi" /> <button class="btn btn-success">Kamar Mandi</button></label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="check"><input type="checkbox" class="icheckbox" name="fasilitas[]" value="Kasur" /> <button class="btn btn-success">Kasur</button></label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="check"><input type="checkbox" class="icheckbox" name="fasilitas[]" value="AC" /> <button class="btn btn-success">AC</button></label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="check"><input type="checkbox" class="icheckbox" name="fasilitas[]" value="Wifi Internet" /> <button class="btn btn-success">Wifi Internet</button></label>
                                                </div>
                                                <div class="col-md-5">
                                                    <label class="check"><input type="checkbox" class="icheckbox" name="fasilitas[]" value="Akses Kunci 24 Jam" /> <button class="btn btn-success">Akses Kunci 24 Jam</button></label>
                                                    <br>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Harga</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="harga_kos" />
                                            </div>
                                            <span class="help-block" style="padding-bottom: 20px;">Contoh: 200000</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Ukuran Kamar</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="ukuran_kamar" />
                                            </div>
                                            <span class="help-block" style="padding-bottom: 20px;">Contoh: 2x2</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Jumlah Kamar</label>
                                        <div class="col-md-9 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="jumlah_kamar" />
                                            </div>
                                            <span class="help-block" style="padding-bottom: 20px;">Contoh: 8</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">FOTO 1</label>
                                        <div class="col-md-9 col-xs-12">
                                            <input type="file" multiple id="file-simple" name="foto_kos_1" />
                                            <span class="help-block" style="padding-bottom: 20px;">Wajib pastikan type foto: jpg, png, jpeg</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">FOTO 2</label>
                                        <div class="col-md-9 col-xs-12">
                                            <input type="file" multiple id="file-simple2" name="foto_kos_2" />
                                            <span class="help-block" style="padding-bottom: 20px;">Wajib pastikan type foto: jpg, png, jpeg</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">FOTO 3</label>
                                        <div class="col-md-9 col-xs-12">
                                            <input type="file" multiple id="file-simple3" name="foto_kos_3" />
                                            <span class="help-block" style="padding-bottom: 20px;">Wajib pastikan type foto: jpg, png, jpeg</span>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <!-- <button class="btn btn-default">Kembali</button> -->
                        <button class="btn btn-primary pull-right" type="submit" name="simpan">Simpan Data</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>