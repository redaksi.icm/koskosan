<?php
include "../../config/koneksi.php";
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
$status = $_SESSION['status'];
$q = mysqli_query($connect, "SELECT * from tb_user where id_user ='$id_user' and status='$status'");
if (mysqli_num_rows($q) == 0) {
    header("location:login/error.php");
} else {
}

if (@$_GET['id'] == 'sukses') {
    echo '<div class="row">
            <div class="col-md-12">
                <div class="alert alert-info" role="alert">
                <a href="home_adm.php?administrator=3" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                <strong>Sukses!</strong> Data berhasil di hapus...
                </div>
            </div>
        </div>';
} else {
    echo '';
}
?>


<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="background: #5e92b9;">
                    <h3 class="panel-title">TAMPILAN DATA PEMILIK</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID PEMILIK</th>
                                <th>NO KTP</th>
                                <th>NAMA</th>
                                <th style="align-items: center">ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampilkan = mysqli_query($connect, "SELECT * FROM tb_pemilik_kos_kontrakan  ORDER BY id_pemilik DESC");
                            foreach ($tampilkan as $data) {

                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data['id_pemilik']; ?></td>
                                    <td><?php echo $data['no_ktp_pemilik']; ?></td>
                                    <td><?php echo $data['nama_pemilik']; ?></td>
                                    <td style="width: 120px;">
                                        <ul class="panel-controls pull-left" style="margin-top: 2px;">
                                            <li><a href="home_adm.php?administrator=5&id=<?php echo $data['id_pemilik']; ?>"><span class="fa fa-eye"></span></a></li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="home_adm.php?administrator=6&id=<?php echo $data['id_pemilik']; ?>" class=""><span class="fa fa-angle-down"></span> Edit data</a></li>
                                                    <li><a href="home_adm.php?administrator=7&id=<?php echo $data['id_pemilik']; ?>" class=""><span class="fa fa-times"></span> Hapus</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>