<?php
include "../../config/koneksi.php";
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
$id_pemilik = $_SESSION['id_pemilik'];
$status = $_SESSION['status'];
$q = mysqli_query($connect, "SELECT * from tb_user where id_user ='$id_user' and status='$status'");
if (mysqli_num_rows($q) == 0) {
    header("location:login/error.php");
} else {
}
?>
<?php
if (@$_GET['id'] == 'sukses') {
    echo '<div class="row">
            <div class="col-md-12">
                <div class="alert alert-info" role="alert">
                <a href="home_adm.php?administrator=9" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                <strong>Sukses!</strong> Data berhasil di hapus...
                </div>
            </div>
        </div>';
} else {
    echo '';
}
?>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="background: #5e92b9;">
                    <h3 class="panel-title">TAMPILAN DATA KOS</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID KOS</th>
                                <th>NAMA KOS</th>
                                <th>NAMA PEMILIK</th>
                                <th style="align-items: center">ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            if ($_SESSION['status'] == 'admin') {
                                $tampilkan = mysqli_query($connect, "SELECT * FROM tb_kos_kontrakan inner join tb_pemilik_kos_kontrakan on tb_kos_kontrakan.id_pemilik = tb_pemilik_kos_kontrakan.id_pemilik where tb_kos_kontrakan.jenis_hunian ='Kos-kosan' ORDER BY id_kos_kontrakan DESC");
                            } else {
                                $tampilkan = mysqli_query($connect, "SELECT * FROM tb_kos_kontrakan inner join tb_pemilik_kos_kontrakan on tb_kos_kontrakan.id_pemilik = tb_pemilik_kos_kontrakan.id_pemilik where tb_kos_kontrakan.jenis_hunian ='Kos-kosan' and tb_pemilik_kos_kontrakan.id_pemilik='$id_pemilik' ORDER BY id_kos_kontrakan DESC");
                            }
                            foreach ($tampilkan as $data) {

                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data['id_kos_kontrakan']; ?></td>
                                    <td><?php echo $data['nama_kos_kontrakan']; ?></td>
                                    <td><?php echo $data['nama_pemilik']; ?></td>
                                    <td>
                                        <ul class="panel-controls pull-left" style="margin-top: 2px;">
                                            <li><a href="home_adm.php?administrator=10&id=<?php echo $data['id_kos_kontrakan']; ?>&j_link=kos"><span class="fa fa-eye"></span></a></li>
                                            <?php if ($_SESSION['status'] == 'admin') { ?>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="home_adm.php?administrator=11&id=<?php echo $data['id_kos_kontrakan']; ?>" class=""><span class="fa fa-angle-down"></span> Edit data</a></li>
                                                        <li><a href="home_adm.php?administrator=12&id=<?php echo $data['id_kos_kontrakan']; ?>" class=""><span class="fa fa-times"></span> Hapus</a></li>
                                                    </ul>
                                                </li>
                                            <?php } else { ?> <?php } ?>
                                        </ul>

                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>