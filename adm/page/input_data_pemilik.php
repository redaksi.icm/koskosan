<?php
include "../../config/koneksi.php";
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
$status = $_SESSION['status'];
$q = mysqli_query($connect, "SELECT * from tb_user where id_user ='$id_user' and status='$status'");
if (mysqli_num_rows($q) == 0) {
    header("location:login/error.php");
} else {
}
?>

<?php

$permitted_chars = '0123456789ABCDFGHOJKLMNOPQRSTUVWXYZ';
$kode_random = substr(str_shuffle($permitted_chars), 0, 7);
$tahun = date("Y");
$time = date("His");

if (isset($_POST['simpan'])) {
    $id_pemilik = $_POST['id_pemilik'];
    $no_ktp = $_POST['no_ktp'];
    $nama_pemilik_kos = $_POST['nama_pemilik'];
    $kelamin = $_POST['kelamin'];
    $email_pemilik = $_POST['email_pemilik'];
    $no_telpon = $_POST['no_telpon'];
    $alamat_pemilik = $_POST['alamat_pemilik'];
    $no_whatsapp = $_POST['no_whatsapp'];

    $ekstensi_diperbolehkan = array('jpg', 'png', 'jpeg');
    $nama_pemilik = $_FILES['foto_pemilik']['name'];
    $nama = $kode_random . '-' . $time . '-' . $nama_pemilik;
    $x = explode('.', $nama);
    $ekstensi = strtolower(end($x));
    $ukuran    = $_FILES['foto_pemilik']['size'];
    $file_tmp = $_FILES['foto_pemilik']['tmp_name'];

    if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {

        move_uploaded_file($file_tmp, '../gambar_adm/' . $nama);

        $no_telpon = $_POST['no_telpon'];
        $simpan = mysqli_query($connect, "INSERT INTO tb_pemilik_kos_kontrakan (id_pemilik, no_ktp_pemilik, nama_pemilik, jenis_kelamin, no_telpon, alamat_pemilik, foto_pemilik, email, no_whatsapp) values ('$id_pemilik','$no_ktp','$nama_pemilik_kos','$kelamin','$no_telpon','$alamat_pemilik','$nama','$email_pemilik','$no_whatsapp')");

        if (!$simpan) {
            echo '
            <script type="text/javascript">
                window.setTimeout(function(){ 
                    window.location.replace("home_adm.php?administrator=2&sukses=404");
                } ,2000); 
            </script>
            <div class="row">
                    <div class="col-md-12"><div class="alert alert-danger" role="alert">
                        <a href="home_adm.php?administrator=2&sukses=404"class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                            <strong>Gagal!</strong> Ada kesalahan, Coba cek data kembali.
                        </div>
                    </div>
                </div> ';
        } else {
            echo '
            <script type="text/javascript">
                window.setTimeout(function(){ 
                    window.location.replace("home_adm.php?administrator=2&sukses=404");
                } ,2000); 
            </script>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info" role="alert">
                    <a href="home_adm.php?administrator=2&sukses=404" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                    <strong>Sukses!</strong> Data berhasil di simpan...
                    </div>
                </div>
            </div>';
        }
    } else {

        echo '
        <script type="text/javascript">
            window.setTimeout(function(){ 
                window.location.replace("home_adm.php?administrator=2&sukses=404");
            } ,2000); 
        </script>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <a href="home_adm.php?administrator=2&sukses=404" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                    <strong>Gagal!</strong> type gambar harus jpg, png, jpeg.
                </div>
            </div>
        </div>';
    }
}
?>

<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="background: #5e92b9;">
                    <h3 class="panel-title">INPUT DATA PEMILIK</h3>
                </div>
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">ID PEMILIK</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" name="id_pemilik" class="form-control" value="<?php echo $time . $kode_random . $tahun; ?>" readonly style="background: #428bca3b; color: black;" />
                                        </div>
                                        <span class="help-block" style="padding-top: 20px;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">NO KTP</label>
                                    <div class="col-md-9 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="number" class="form-control" name="no_ktp" />
                                        </div>
                                        <span class="help-block" style="padding-top: 20px;"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">NAMA PEMILIK</label>
                                    <div class="col-md-9 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" class="form-control" name="nama_pemilik" />
                                        </div>
                                        <span class="help-block" style="padding-top: 20px;"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">JENIS KELAMIN</label>
                                    <div class="col-md-9 col-xs-12">
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <label class="check"><input type="radio" class="iradio" name="kelamin" value="Laki-laki" /> Laki-laki</label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="check"><input type="radio" class="iradio" name="kelamin" value="Perempuan" checked="checked" /> Perempuan</label>
                                            </div>
                                        </div>
                                        <span class="help-block" style="padding-top: 20px;"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">EMAIL</label>
                                    <div class="col-md-9 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" class="form-control" name="email_pemilik" />
                                        </div>
                                        <span class="help-block" style="padding-top: 20px;"></span>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">NO TELPON</label>
                                    <div class="col-md-9 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" class="form-control" name="no_telpon" />
                                        </div>
                                        <span class="help-block" style="padding-top: 20px;"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">NO WHATSAPP</label>
                                    <div class="col-md-9 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" class="form-control" value="62" name="no_whatsapp" />
                                        </div>
                                        <span class="help-block">Nomor harus terdaftar di app whatsapp</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">ALAMAT PEMILIK</label>
                                    <div class="col-md-9 col-xs-12">
                                        <textarea class="form-control" rows="5" name="alamat_pemilik"></textarea>
                                        <span class="help-block" style="padding-top: 20px;"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">FOTO PEMILIK</label>
                                    <div class="col-md-9 col-xs-12">
                                        <input type="file" multiple id="file-simple" name="foto_pemilik" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <!-- <button class="btn btn-default">Kembali</button> -->
                        <button class="btn btn-primary pull-right" type="submit" name="simpan">Simpan Data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>