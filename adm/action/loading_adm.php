<?php
// include "koneksi.php";

if (isset($_GET["administrator"])) {
    $p = $_GET["administrator"];
} else {
    $p = 1;
}
switch ($p) {

    case "1":
        require("../page/beranda.php");
        break;

    case "2":
        require("../page/input_data_pemilik.php");
        break;

    case "3":
        require("../page/tampilan_pemilik.php");
        break;

    case "4":
        require("logout.php");
        break;

    case "5":
        require("../page/view_data_pemilik.php");
        break;

    case "6":
        require("../page/edit_data_pemilik.php");
        break;

    case "7":
        require("../page/delete_pemilik.php");
        break;

    case "8":
        require("../page/input_kos-kosan.php");
        break;

    case "9":
        require("../page/tampilan_data_kos.php");
        break;

    case "10":
        require("../page/view_kos_kontrakan.php");
        break;

    case "11":
        require("../page/edit_kos_kontrakan.php");
        break;

    case "12":
        require("../page/delete_kos_kontrakan.php");
        break;


    case "13":
        require("../page/tampilan_data_kontrakan.php");
        break;

    case "14":
        require("../page/invoice.php");
        break;

    case "15":
        require("../page/action_selesai.php");
        break;

    case "16":
        require("../page/action_batal.php");
        break;

    case "17":
        require("../page/delete_data_booking.php");
        break;

    case "18":
        require("../page/arsip_data.php");
        break;

    case "19":
        require("../page/data_user.php");
        break;

    case "20":
        require("../page/tambah_user.php");
        break;

    case "21":
        require("../page/update_user.php");
        break;

    case "22":
        require("../page/delete_user.php");
        break;
}
