<!DOCTYPE html>
<html class="no-js" lang="zxx">
    <head>
		<!-- Meta Tags -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="keywords" content="Site keywords here">
		<meta name="description" content="">
		<meta name='copyright' content=''>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<!-- Title -->
		<title>Kos-Kosan</title>
		
		<!-- Favicon -->
		<link rel="icon" type="image/png" href="desain/logo/kos-kosan.png">
		
		<!-- Web Font -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="desain/css/bootstrap.min.css">
		<!-- Font Awesome CSS -->
        <link rel="stylesheet" href="desain/css/font-awesome.min.css">
		<!-- Nice Select CSS -->
        <link rel="stylesheet" href="desain/css/niceselect.css">
		<!-- Fancy Box CSS -->
        <link rel="stylesheet" href="desain/css/jquery.fancybox.min.css">
		<!-- Fancy Box CSS -->
        <link rel="stylesheet" href="desain/css/cube-portfolio.min.css">
		<!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="desain/css/owl.carousel.min.css">
		<!-- Animate CSS -->
        <link rel="stylesheet" href="desain/css/animate.min.css">
		<!-- Slick Nav CSS -->
        <link rel="stylesheet" href="desain/css/slicknav.min.css">
        <!-- Magnific Popup -->
		<link rel="stylesheet" href="desain/css/magnific-popup.css">
		
		<!-- Eduland Stylesheet -->
        <link rel="stylesheet" href="desain/css/normalize.css">
        <link rel="stylesheet" href="desain/style.css">
        <link rel="stylesheet" href="desain/css/responsive.css">
		
		<!-- Eduland Colors -->
		<link rel="stylesheet" href="desain/css/colors/color1.css">
		
    </head>
    <body>
	
		<!-- Header -->
		<header class="header">
			<!-- Header Inner -->
			<div class="header-inner overlay">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-12">
							<!-- Logo -->
							<div class="logo">
								<a href="index.php"><img src="desain/logo/kos-kosan.png" alt="#" style="max-width: 40px;width: 50px;">Kos-kosan</a>
							</div>
							<!--/ End Logo -->
							<div class="mobile-menu"></div>
						</div>
						<div class="col-lg-9 col-md-9 col-12">
							<div class="menu-bar">
								<nav class="navbar navbar-default">
									<div class="navbar-collapse">
										<!-- Main Menu -->
										<ul id="nav" class="nav menu navbar-nav">
											<li class="active"><a href="index.php"><i class="fa fa-windows"></i>Beranda</a></li>
											<li><a href="index.php?kosan=2"><i class="fa fa-home"></i> Kos-kosan</a></li>
											<li><a href="index.php?kosan=3"><i class="fa fa-building-o"></i>Kontrakan</a></li>
											<!-- <li><a href="#"><i class="fa fa-clone"></i>Pages</a> 
												<ul class="dropdown">
													<li><a href="teachers.html">Teachers</a></li>
												</ul>
											</li> -->											
											<li><a href="index.php?kosan=8"><i class="fa fa-user"></i>Cek Booking</a></li>
											<li><a href="index.php?kosan=10"><i class="fa fa-search"></i>Cari</a></li>
										</ul>
										<!-- End Main Menu -->
									</div> 
								</nav>
								<!-- Search Area -->
								<!-- <div class="search-area">
									<a href="#header" class="icon"><i class="fa fa-search"></i></a>
									<form class="search-form">
										<input type="text" placeholder="Cek Booking" name="search">
										<button value="search " type="submit"><i class="fa fa-search"></i></button>
									</form>
								</div>	 -->
								<!-- End Search Area-->
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--/ End Header Inner -->
		</header>
		<!--/ End Header -->
		
		<!-- Slider Area -->
		
		<!--/ End Slider Area -->
		
	<!-- loading -->
	<?php include 'config/loading.php';?>

		<!-- Clients CSS -->
		<div class="clients">
			<div class="container">
				<div class="row">
					<!-- <div class="col-lg-4 col-md-4 col-12">
						<div class="text-content">
							<h4>Our Awesome Clients!</h4>
							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Et harum quidem</p>
						</div>
					</div>
					<div class="col-lg-8 col-md-8 col-12">
						<div class="client-slider">
							<div class="single-slider">
								<a href="#"><img src="desain/images/client1.png" alt="#"></a>
							</div>
							<div class="single-slider">
								<a href="#"><img src="desain/images/client2.png" alt="#"></a>
							</div>
							<div class="single-slider">
								<a href="#"><img src="desain/images/client3.png" alt="#"></a>
							</div>
							<div class="single-slider">
								<a href="#"><img src="desain/images/client4.png" alt="#"></a>
							</div>
							<div class="single-slider">
								<a href="#"><img src="desain/images/client5.png" alt="#"></a>
							</div>
							<div class="single-slider">
								<a href="#"><img src="desain/images/client1.png" alt="#"></a>
							</div>
							<div class="single-slider">
								<a href="#"><img src="desain/images/client2.png" alt="#"></a>
							</div>
						</div>
					</div> -->
				</div>
			</div>
		</div>
		<!--/ End Clients CSS -->
		
		<!-- Footer -->
		<footer class="footer section">
			<!-- Footer Top -->
			<!--/ End Footer Top -->
			<!-- Footer Bottom -->
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<!-- Copyright -->
							<div class="copyright">
								<p>© Copyright Eduland 2019. Design & Development by <a href="http://themelamp.com">www.themelamp.com</a>, Theme Release By <a href="http://codeglim.com">www.codeglim.com</a></p>
							</div>
							<!--/ End Copyright -->
						</div>
					</div>
				</div>
			</div>
			<!--/ End Footer Bottom -->
		</footer>
		<!--/ End Footer -->
		
		<!-- Jquery JS-->
        <script src="desain/js/jquery.min.js"></script>
        <script src="desain/js/jquery-migrate.min.js"></script>
		<!-- Colors JS-->
        <script src="desain/js/colors.js"></script>
		<!-- Popper JS-->
        <script src="desain/js/popper.min.js"></script>
		<!-- Bootstrap JS-->
        <script src="desain/js/bootstrap.min.js"></script>
		<!-- Owl Carousel JS-->
        <script src="desain/js/owl.carousel.min.js"></script>
		<!-- Jquery Steller JS -->
		<script src="desain/js/jquery.stellar.min.js"></script>
		<!-- Final Countdown JS -->
		<script src="desain/js/finalcountdown.min.js"></script>
		<!-- Fancy Box JS-->
		<script src="desain/js/facnybox.min.js"></script>
		<!-- Magnific Popup JS-->
		<script src="desain/js/jquery.magnific-popup.min.js"></script>
		<!-- Circle Progress JS -->
		<script src="desain/js/circle-progress.min.js"></script>
		<!-- Nice Select JS -->
		<script src="desain/js/niceselect.js"></script>
		<!-- Jquery Steller JS-->
        <script src="desain/js/jquery.stellar.min.js"></script>
		<!-- Jquery Steller JS-->
        <script src="desain/js/cube-portfolio.min.js"></script>
		<!-- Slick Nav JS-->
        <script src="desain/js/slicknav.min.js"></script>
		<!-- Easing JS-->
        <script src="desain/js/easing.min.js"></script>
		<!-- Waypoints JS-->
        <script src="desain/js/waypoints.min.js"></script>
		<!-- Counter Up JS -->
		<script src="desain/js/jquery.counterup.min.js"></script>
		<!-- Scroll Up JS-->
        <script src="desain/js/jquery.scrollUp.min.js"></script>
		<!-- Gmaps JS-->
		<script src="desain/js/gmaps.min.js"></script>
		<!-- Main JS-->
        <script src="desain/js/main.js"></script>
    </body>
</html>