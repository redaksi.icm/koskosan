<section class="home-slider" style="height: 100px;">
    <div class="slider-active">
        <!-- Single Slider -->
        <div class="single-slider overlay">
            <div class="slider-image" style="background-image:url('desain/logo/rumah.jpg')"></div>
        </div>
        <!--/ End Single Slider -->
    </div>
</section>
<?php
include "config/koneksi.php";
// $tampilkan = mysqli_query($connect, "SELECT * FROM tb_kos_kontrakan where id_kos_kontrakan='$_GET[detail]'");
$tampilkan = mysqli_query($connect, "SELECT * FROM tb_booking where id_kos_kontrakan = '$_GET[booking]' and no_ktp='$_GET[id]'");

foreach ($tampilkan as $data) {

    $id = $data['id_kos_kontrakan'];

?>
    <section id="contact" class="contact section">
        <div class="container">
            <hr />
            <center>
                <h6>SELAMAT, BOOKING SUDAH BERHASIL...</h6>
            </center>
            <hr>
            <div class="row" style="background: beige;border: dashed;border-top: solid;border-color: #05c46b;">
                <div class="col-md-12" style="background: #ffffff;">
                    <div class="contact-info">
                        <h2>INVOICE #<font color='red'><?php echo $data['no_booking']; ?></font>
                        </h2>
                        <i style="font-size: 13px; float: right;color:red;">Info: Foto/screenshots, Bukti Ini</i>
                    </div>
                </div>
                <div class="col-md-12" style="border-bottom: solid;border-color: darksalmon;"></div>
                <div class="col-lg-4 col-md-4" style="background: #ebebeb;padding-bottom: 60px;">
                    <div class="contact-right" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                        <!-- Contact-Info -->
                        <div class="contact-info">
                            <h3>Nama : <?php echo $data['nama_sesuai_ktp']; ?></h3>
                            <p>No Ktp : <?php echo $data['no_ktp']; ?></p>
                            <p>Jenis Kelamin : <?php echo $data['jenis_kelamin']; ?></p>
                        </div>
                        <!-- Contact-Info -->
                        <div class="contact-info">
                            <h3>Information Kontak</h3>
                            <p>Alamat : <?php echo $data['alamat_sesuai_ktp']; ?></p>
                            <p>No Telpon : <?php echo $data['no_telpon']; ?></p>
                            <p>Whatsapp : <?php echo $data['no_whatsapp']; ?></p>
                            <p>Email : <?php echo $data['email']; ?></p>
                        </div>


                        <!-- Contact-Info -->
                    </div>
                </div>
                <?php
                $tampilkan1 = mysqli_query($connect, "SELECT * FROM tb_kos_kontrakan inner join tb_pemilik_kos_kontrakan on tb_kos_kontrakan.id_pemilik = tb_pemilik_kos_kontrakan.id_pemilik where tb_kos_kontrakan.id_kos_kontrakan = '$id'");

                foreach ($tampilkan1 as $data1) {
                ?>
                    <div class="col-lg-4 col-md-4" style="background: #ffffff;">
                        <div class="contact-right" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                            <!-- Contact-Info -->
                            <div class="contact-info">
                                <h3>Nama Pemilik : <?php echo $data1['nama_pemilik']; ?></h3>
                            </div>
                            <!-- Contact-Info -->
                            <div class="contact-info">
                                <h3>Information Kontak</h3>
                                <p>Alamat : <?php echo $data1['alamat_pemilik']; ?></p>
                                <p>No Telpon : <?php echo $data1['no_telpon']; ?></p>
                                <p>Whatsapp : <?php echo $data1['no_whatsapp']; ?></p>
                                <p>Email : <?php echo $data1['email']; ?></p>
                            </div>
                            <div class="contact-info">
                                <h3>Detail Kos/Kontrakan</h3>
                                <p>ID : <?php echo $data1['id_kos_kontrakan']; ?></p>
                                <p>Jenis Hunian : <?php echo $data1['jenis_hunian']; ?></p>

                            </div>
                            <!-- Contact-Info -->

                        </div>
                    </div>
                <?php } ?>

                <div class="col-lg-4 col-md-4" style="background: #ffffff;">
                    <div class="contact-right" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
                        <!-- Contact-Info -->
                        <div class="contact-info">
                            <p style="padding-left: 100px;"> <img src="desain/logo/kos-kosan.png" alt="" style="max-width: 200px;max-height: 130px;"> </p>
                        </div>
                        <!-- Contact-Info -->

                        <div class="contact-info">
                            <p><b>Invoice Number : <?php echo $data['no_booking']; ?></b></p>
                            <p>TGL Booking : <?php echo date('d-m-Y:h:i:s',  strtotime($data['tgl_booking'])) ?></p>
                            <p>Batas Konfirmasi Booking : <?php echo date('d-m-Y h:i:s', strtotime('+3 days', strtotime($data['tgl_booking']))) ?></p>
                            <p>Total Biaya : <?php echo number_format($data1['harga'], 0, ',', '.'); ?> /<?php echo $data1['jenis_sewa']; ?></p>
                        </div>
                        <!-- Contact-Info -->
                    </div>
                </div>

                <div class="col-md-12" style="background: #15df2b57;border-top: inset;">
                    <div class="contact-info">
                        <p>Catatan Baca Dulu: </p>
                        <p style="font-size: 11px">1. Konfirmasi Booking kepada pemilik kos/kontrakan melalui info kontak yang tertera di atas selama kurang 24 jam.</p>
                        <p style="font-size: 11px">2. Waktu Konfirmasi, selama 1 hari dari waktu pembookingan kepada pemilik kos/kontrakan.</p>
                        <p style="font-size: 11px">3. Booking outomatis di batalkan setelah melewati waktu konfirmasi.</p>
                        <p style="font-size: 11px">4. Transaksi pembayaran bisa di diskusikan dengan pemilik kos/kontrakan saat konfirmasi pembookingan.</p>
                    </div>
                </div>

            </div>
            <hr />
        </div>
    </section>
    <?php

    // untuk konsumen
    $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    ini_set('display_errors', 1);
    error_reporting(E_ALL);
    $from = "koskosanpadang@gmail.com";
    $to = "$data[email]";
    $subject = "Invoice Booking Kos-kosan";
    $message = "Terimaksih booking anda berhasil dengan No. Invoice " . $data['no_booking'] . "Klik link untuk cek invoice " . "<a href='".$url."'>Klik link disini </a>" . "Silahkan lakukan pembayaran dan hubungi pemilik kos-kosan kami ya...";
    $headers = "From:" . $from;
    mail($to, $subject, $message, $headers);

    // untuk pemilik
    $url_login_adm = "https://kospadangutara.000webhostapp.com/adm/";
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
    $from = "koskosanpadang@gmail.com";
    $to = "$data1[email]";
    $subject = "Invoice Booking Kos-kosan";
    $message = "booking berhasil dengan No. Invoice " . $data['no_booking'] . "Klik link untuk cek invoice " . "<a href='".$url."'>Klik link disini </a>" . "Silahkan login ke portal pemilik untuk mengkonfirmasi pemesanan..  " . "<a href='".$url_login_adm."'>Klik link Portal </a>";
    $headers = "From:" . $from;
    mail($to, $subject, $message, $headers);

    ?>

<?php } ?>