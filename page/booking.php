<section class="home-slider" style="height: 200px;">
    <div class="slider-active">
        <!-- Single Slider -->
        <div class="single-slider overlay">
            <div class="slider-image" style="background-image:url('desain/logo/rumah.jpg')"></div>
        </div>
        <!--/ End Single Slider -->
    </div>
</section>
<?php
include "config/koneksi.php";

if (@$_GET['gagal'] == 'gagal') {

    echo '  <section id="contact" class="contact section" style="padding-top: 30px;padding-bottom: 0px;">
                <div class="container">
                    <div class="col-lg-12 col-md-12 col-12">  
                        <div class="form-group">
                            <div class="button">
                           <i style="color: #ff0000;"> Warning: Anda belum menyelesaikan Booking, Cek Disini !!  </i> 
                             <a class="primary" target=_blank href="index.php?kosan=8"> Lihat disini.. </a>
                            </div>
                        </div>                            
                    </div>
                </div>
            </section>';
} else {
}

@$id = $_GET['id'];
$tampilkan = mysqli_query($connect, "SELECT * FROM tb_kos_kontrakan inner join tb_pemilik_kos_kontrakan on tb_kos_kontrakan.id_pemilik = tb_pemilik_kos_kontrakan.id_pemilik where tb_kos_kontrakan.id_kos_kontrakan = '$id'");


foreach ($tampilkan as $data) {

    $hobi = explode(",", $data['jenis_fasilitas']);
?>
    <section id="contact" class="contact section" style="padding-top: 0px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12">
                    <div class="section-title bg">
                        <h2><span>BOOKING</span></h2>
                    </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-lg-8 col-md-8 col-12" style="border-right-style: dotted;">
                    <div class="form-head">
                        <!-- Contact Form -->
                        <form class="form" action="index.php?kosan=6" method="POST">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-12">
                                    <label for="">NAMA SESUAI KTP</label>
                                    <div class="form-group">
                                        <i class="fa fa-user"></i>
                                        <input name="nama_sesuai_ktp" type="text" required>
                                        <input name="id_kos_kontrakan" type="hidden" value="<?php echo $data['id_kos_kontrakan']; ?>">
                                        <input name="id_booking" type="hidden" value="<?php echo 'BK' . date('dmYhis'); ?>">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <label for="">NO KTP</label>
                                    <div class="form-group">
                                        <i class="fa fa-credit-card"></i>
                                        <input name="no_ktp" type="text" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <label for="">ALAMAT EMAIL</label>
                                    <div class="form-group">
                                        <i class="fa fa-envelope"></i>
                                        <input name="email" type="text" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <label for="">NO TELPON</label>
                                    <div class="form-group">
                                        <i class="fa fa-keyboard-o"></i>
                                        <input name="no_telpon" type="number" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <label for="">NO WHATSAPP</label>
                                    <div class="form-group">
                                        <i class="fa fa-keyboard-o"></i>
                                        <input name="no_whatsapp" type="number" value="62" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <label for="">JENIS KELAMIN</label>
                                    <!-- <div class="form-group"> -->
                                    <!-- <select class="form-control" name="jenis_kelamin" required>
                                            <option value="0">PILIH...</option>
                                            <option value="Laki-laki">LAKI-LAKI</option>
                                            <option value="Perempuan">PEREMPUAN</option>
                                        </select> -->
                                    <div class="row">
                                        <input type="hidden" value="<?php echo $data['jenis_penghuni']; ?>" name="jenis_hunian">
                                        <div class="col-md-4">
                                            <label class="container">Laki-laki
                                                <input type="radio" name="jenis_kelamin" value="Laki-laki">
                                            </label>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="container">Perempuan
                                                <input type="radio" name="jenis_kelamin" value="Perempuan">
                                            </label>
                                        </div>
                                    </div>


                                    <!-- </div> -->
                                </div>
                                <div class="col-12">
                                    <label for="">ALAMAT SESUAI KTP</label>
                                    <div class="form-group message">
                                        <i class="fa fa-pencil"></i>
                                        <textarea name="alamat_sesuai_ktp" required></textarea>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-12">
                    <div class="contact-right">
                        <!-- Contact-Info -->
                        <div class="single-event">
                            <ul class="list-group">
                                <li class="list-group-item" style="color: #f8f8f8;background-color: #3b4b61;"><b>ID BOOKING : <?php echo 'BK' . date('dmYhis'); ?></b></li>
                                <li class="list-group-item"><b>Jumlah Booking : 1</b></li>
                                <li class="list-group-item"><b>Bayar : Rp. <?php echo number_format($data['harga'], 0, ',', '.'); ?> /<?php echo $data['jenis_sewa']; ?> </b></li>
                                <li class="list-group-item"><span><img src="gambar/pemilik.png" alt="" width="30px"></span>
                                    <span>
                                        <span style="font-size: 15px"> Pemilik Kos</span>
                                        <span style="font-size: 11px"> : <?php echo $data['nama_pemilik']; ?></span>
                                    </span>
                                </li>
                                <li class="list-group-item">
                                    Whatsapp : <?php echo $data['no_whatsapp']; ?>
                                </li>
                                <li class="list-group-item">
                                    Alamat <?php echo $data['alamat_kos_kontrakan']; ?>
                                </li>
                                <li class="list-group-item">
                                    Jenis Penghuni <?php echo $data['jenis_penghuni']; ?>
                                </li>
                                <li class="list-group-item">
                                    <span class="entry-date-time"><i class="fa fa-clock-o" aria-hidden="true"></i> Tanggal Booking <?php echo date('d-m-Y'); ?></span>
                                </li>
                            </ul>
                        </div>
                        <!-- Contact-Info -->
                    </div>
                    <br>
                    <div class="col-12">
                        <div class="form-group">
                            <div class="button">
                                <button type="submit" class="btn primary" onClick="return confirm('Apakah anda ingin membooking hunian ini, Klik oke kalau anda setuju??')">BOOKING SEKARANG</button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </section>
<?php } ?>