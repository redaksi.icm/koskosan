<section class="home-slider" style="height: 100px;">
    <div class="slider-active">
        <!-- Single Slider -->
        <div class="single-slider overlay">
            <div class="slider-image" style="background-image:url('desain/logo/rumah.jpg')"></div>
        </div>
        <!--/ End Single Slider -->
    </div>
</section>

<section id="contact" class="contact section" style="padding-top: 30px;padding-bottom: 0px;">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="">
                <form method="POST" action="">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">Kategori</label> <br>
                                <select class="form-control" name="jenis_penghuni">
                                    <option value="0">PILIH...</option>
                                    <option value="Putra">LAKI-LAKI</option>
                                    <option value="Putri">PEREMPUAN</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Cari Lokasi</label> <br>
                                <input type="text" placeholder="Cari lokasi hunian" name="carkan" value="" style="border-color: #0bbf3b;">
                                <button type="submit" name="simpan" style="height: 34px;width: 65px;background: #05c46b;"><i class="fa fa-search"></i></button>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php
if (isset($_POST['simpan'])) {
    $jenis_penghuni = $_POST['jenis_penghuni'];
    $carkan = $_POST['carkan'];

    if ($jenis_penghuni === 0) {
        $hunikannakan = '-';
    } else {
        $hunikannakan = $jenis_penghuni;
    }

    echo '<section id="contact" class="contact section" style="padding-top: 30px;padding-bottom: 0px;">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="row">

                <div class="ol-md-4">
                    <h4>Kategori : ' . $hunikannakan . '</h4>
                </div>
                <div class="ol-md-6">
                    <h4>&nbsp;&nbsp;&nbsp; Lokasi : ' . $carkan . '</h4>
                </div>
            </div>
        </div>
    </div>
</section>';
}
?>





<section class="courses section" style="background: #ddd6d663;padding-top: 15px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-12">
                <div class="section-title bg">
                    <h2>KOS-KOSAN</h2>
                    <div class="icon"><i class="fa fa-home"></i></div>
                </div>
            </div>
        </div>
        <div class="row">

            <?php

            if (isset($_POST['simpan'])) {
                include "config/koneksi.php";

                $jenis_penghuni = $_POST['jenis_penghuni'];
                $carkan = $_POST['carkan'];

                $kata_kunci = $connect->real_escape_string(htmlentities(trim($carkan)));

                $where = "";
                //membuat variabel $kata_kunci_split untuk memecah kata kunci setiap ada spasi
                $kata_kunci_split = preg_split('/[\s]+/', $kata_kunci);
                //menghitung jumlah kata kunci dari split di atas
                $total_kata_kunci = count($kata_kunci_split);
                //melakukan perulangan sebanyak kata kunci yang di masukkan
                foreach ($kata_kunci_split as $key => $kunci) {
                    //set variabel $where untuk query nanti
                    $where .= "alamat_kos_kontrakan LIKE '%$kunci%'";
                    //jika kata kunci lebih dari 1 (2 dan seterusnya) maka di tambahkan OR di perulangannya
                    if ($key != ($total_kata_kunci - 1)) {
                        $where .= " OR ";
                    }
                }

                if (($jenis_penghuni == 'Putra' or $jenis_penghuni == 'Putri') and !empty($carkan)) {
                    // echo 'cari kelamin dan alamat';

                    $tampilkan = mysqli_query($connect, "SELECT * FROM tb_kos_kontrakan where jenis_penghuni='$jenis_penghuni' and $where  order by id_kos_kontrakan desc");
                } else if ($jenis_penghuni == 0 and !empty($carkan)) {
                    // echo 'cari alamat';

                    $tampilkan = mysqli_query($connect, "SELECT * FROM tb_kos_kontrakan where $where order by id_kos_kontrakan desc");
                } else if ($jenis_penghuni == 'Putra' or $jenis_penghuni == 'Putri') {
                    // echo 'cari kelamin';

                    $tampilkan = mysqli_query($connect, "SELECT * FROM tb_kos_kontrakan where jenis_penghuni='$jenis_penghuni'  order by id_kos_kontrakan desc");
                } else {
                    echo "<script>alert('Isi salah satu opsi');window.location='index.php?kosan=10'</script>";
                }


                $cari_jumlah = mysqli_num_rows($tampilkan);

                if (empty($cari_jumlah)) {
                    echo "<center><section id='contact' class='contact section' style='padding-top: 60px;padding-bottom: 60px;padding-left: 300px;'>
                                <div class='container'>
                                    <div class='col-lg-12 col-md-12 col-12'>
                                        <img src='gambar/data_found.png' style='display: block; margin: auto;'>
                                    </div>
                                </div>
                            </section></center>";
                }

                while ($data = mysqli_fetch_array($tampilkan)) {


            ?>
                    <div class="col-lg-3 col-md-6 col-12">
                        <!-- Single Course -->
                        <div class="single-course" style="padding: 4px;">
                            <!-- Course Head -->
                            <div class="course-head overlay">
                                <img src="adm/gambar_adm/gambar_kos_kontrakan/<?php echo $data['gambar_kos_kontrakan1']; ?>" alt="#" style="height: 190px">
                                <a target="_blank" href="index.php?kosan=4&detail=<?php echo $data['id_kos_kontrakan']; ?>" class="btn white primary" style="font-size: 9px;">Booking Sekarang</a>
                            </div>
                            <!-- Course Body -->
                            <div class="course-body">
                                <div class="name-price">
                                    <!-- <div class="teacher-info">
                                <img src="desain/images/author2.jpg" alt="#" style="height: 190px">
                                <h4 class="title">Jenola Protan</h4>
                            </div> -->
                                    <span class="price" style="font-size: 9px;"> Rp. <?php echo number_format($data['harga'], 0, ',', '.'); ?>/ <?php echo $data['jenis_sewa']; ?></span>
                                </div>
                                <!-- <h4 class="c-title"><a href="course-single.html">Learn Web Developments Course</a></h4> -->
                                <p style="padding-top: 60px;font-size: 13px;color:#093e21;font-family: monospace;"><?php echo $data['nama_kos_kontrakan']; ?></p>
                            </div>
                            <!-- Course Meta -->
                            <div class="course-meta">
                                <!-- Rattings -->
                                <!-- <ul class="rattings">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-o"></i></li>
                            <li><i class="fa fa-star-o"></i></li>
                            <li class="point"><span>3.9</span></li>
                        </ul> -->
                                <!-- Course Info -->
                                <div class="course-info">
                                    <span style="color: brown;font-size: 11px;font-family: monospace;"><i class="fa fa-users"></i><?php echo $data['jenis_penghuni']; ?></span>
                                    <span style="color: #9d8c8c;font-size: 11px;font-family: monospace;"><i class="fa fa-clock-o"></i><?php echo $data['tgl_upload']; ?></span>
                                    <span style="color: #9d8c8c;font-size: 11px;font-family: monospace;"><i class="fa fa-home"></i>Type: <?php echo $data['ukuran_kamar']; ?></span>

                                </div>
                            </div>
                            <!--/ End Course Meta -->
                        </div>
                        <!--/ End Single Course -->
                    </div>
            <?php       }
            } ?>
        </div>

        <!-- <section id='contact" class="contact section" style="padding-top: 60px;padding-bottom: 60px;">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-12">
                    <img src="gambar/data_found.png" style="display: block; margin: auto;">
                </div>
            </div>
        </section> -->


        <!-- batas -->

        <!-- <div class="row">
            <div class="col-lg-6 offset-lg-3 col-12">
                <br>
                <div class="section-title bg">
                    <div class="icon"><i class="fa fa-share"></i></div>
                    <a href="#">
                        <div class="value off">Lebih banyak</div>
                    </a>

                </div>
            </div>
        </div> -->

    </div>
</section>