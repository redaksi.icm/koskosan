<section class="home-slider">
    <div class="slider-active">
        <!-- Single Slider -->
        <div class="single-slider overlay">
            <div class="slider-image" style="background-image:url('desain/logo/rumah.jpg')"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-10 col-12">
                        <!-- Slider Content -->
                        <div class="slider-content">
                            <h1 class="slider-title" style="background: #be4e4e"><span>Mau cari kos?</span>SI KOS &<b>KONTRAKAN</b></h1>
                            <p class="slider-text">
                                Fitur Pencarian kosan di Kos-kosan:
                                <i>kami menyediakan informasi menyangkut dengan kos-kosan.</i>
                                <i>serta mempermudah anda untuk mengetahui harga dan tempat kos-kosan yang murah</i>
                            </p>
                            <!-- Button -->
                            <div class="button">
                                <a href="index.php?kosan=2" class="btn white">Kos-kosan </a>
                                <a href="index.php?kosan=3" class="btn white primary">Kontrakan</a>
                            </div>
                            <!--/ End Button -->
                        </div>
                        <!--/ End Slider Content -->
                    </div>
                </div>
            </div>
        </div>
        <!--/ End Single Slider -->
    </div>
</section>
<section class="courses section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-12">
                <div class="section-title bg">
                    <h2>Kos<span>-Kosan</span></h2>
                    <p>Mau cari kos?</p>
                    <div class="icon"><i class="fa fa-home"></i></div>
                </div>
            </div>
        </div>
        <div class="row">

            <?php
            include "config/koneksi.php";
            $tampilkan = mysqli_query($connect, "SELECT * FROM tb_kos_kontrakan where tb_kos_kontrakan.jenis_hunian='Kos-kosan' order by id_kos_kontrakan desc limit 12");

            foreach ($tampilkan as $data) {

            ?>
                <div class="col-lg-3 col-md-6 col-12">
                    <!-- Single Course -->
                    <div class="single-course" style="padding: 4px;">
                        <!-- Course Head -->
                        <div class="course-head overlay">
                            <img src="adm/gambar_adm/gambar_kos_kontrakan/<?php echo $data['gambar_kos_kontrakan1']; ?>" alt="#" style="height: 190px">
                            <a target="_blank" href="index.php?kosan=4&detail=<?php echo $data['id_kos_kontrakan']; ?>" class="btn white primary" style="font-size: 9px;">Booking Sekarang</a>
                        </div>
                        <!-- Course Body -->
                        <div class="course-body">
                            <div class="name-price">
                                <!-- <div class="teacher-info">
                                <img src="desain/images/author2.jpg" alt="#" style="height: 190px">
                                <h4 class="title">Jenola Protan</h4>
                            </div> -->
                                <span class="price" style="font-size: 9px;"> Rp. <?php echo number_format($data['harga'], 0, ',', '.'); ?>/ <?php echo $data['jenis_sewa']; ?></span>
                            </div>
                            <!-- <h4 class="c-title"><a href="course-single.html">Learn Web Developments Course</a></h4> -->
                            <p style="padding-top: 60px;font-size: 13px;color:#093e21;font-family: monospace;"><?php echo $data['nama_kos_kontrakan']; ?></p>
                        </div>
                        <!-- Course Meta -->
                        <div class="course-meta">
                            <!-- Rattings -->
                            <!-- <ul class="rattings">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-o"></i></li>
                            <li><i class="fa fa-star-o"></i></li>
                            <li class="point"><span>3.9</span></li>
                        </ul> -->
                            <!-- Course Info -->
                            <div class="course-info">
                                <span style="color: brown;font-size: 11px;font-family: monospace;"><i class="fa fa-users"></i><?php echo $data['jenis_penghuni']; ?></span>
                                <span style="color: #9d8c8c;font-size: 11px;font-family: monospace;"><i class="fa fa-clock-o"></i><?php echo $data['tgl_upload']; ?></span>
                                <span style="color: #9d8c8c;font-size: 11px;font-family: monospace;"><i class="fa fa-home"></i>Type: <?php echo $data['ukuran_kamar']; ?></span>

                            </div>
                        </div>
                        <!--/ End Course Meta -->
                    </div>
                    <!--/ End Single Course -->
                </div>
            <?php } ?>
        </div>
        <!-- batas -->

        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-12">
                <br>
                <div class="section-title bg">
                    <div class="icon"><i class="fa fa-share"></i></div>
                    <a href="#">
                        <!-- <div class="value off">Lebih banyak</div> -->
                    </a>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- batas kontrakan -->

<section class="courses section" style="background: #ddd6d663;">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-12">
                <div class="section-title bg">
                    <h2>Kontrakan<span></span></h2>
                    <p>Mau cari kontrakan?</p>
                    <div class="icon"><i class="fa fa-home"></i></div>
                </div>
            </div>
        </div>
        <div class="row">

            <?php
            include "config/koneksi.php";
            $tampilkan = mysqli_query($connect, "SELECT * FROM tb_kos_kontrakan where tb_kos_kontrakan.jenis_hunian='kontrakan' order by tgl_upload desc limit 12");

            foreach ($tampilkan as $data) {

            ?>
                <div class="col-lg-3 col-md-6 col-12">
                    <!-- Single Course -->
                    <div class="single-course" style="padding: 4px;">
                        <!-- Course Head -->
                        <div class="course-head overlay">
                            <img src="adm/gambar_adm/gambar_kos_kontrakan/<?php echo $data['gambar_kos_kontrakan1']; ?>" alt="#" style="height: 190px">
                            <a target="_blank" href="index.php?kosan=4&detail=<?php echo $data['id_kos_kontrakan']; ?>" class="btn white primary" style="font-size: 9px;">Booking Sekarang</a>
                        </div>
                        <!-- Course Body -->
                        <div class="course-body">
                            <div class="name-price">
                                <!-- <div class="teacher-info">
                                <img src="desain/images/author2.jpg" alt="#" style="height: 190px">
                                <h4 class="title">Jenola Protan</h4>
                            </div> -->
                                <span class="price" style="font-size: 9px;">Rp. <?php echo number_format($data['harga'], 0, ',', '.'); ?>/ <?php echo $data['jenis_sewa']; ?></span>
                            </div>
                            <!-- <h4 class="c-title"><a href="course-single.html">Learn Web Developments Course</a></h4> -->
                            <p style="padding-top: 60px;font-size: 13px;color:#093e21;font-family: monospace;"><?php echo $data['nama_kos_kontrakan']; ?></p>
                        </div>
                        <!-- Course Meta -->
                        <div class="course-meta">
                            <!-- Rattings -->
                            <!-- <ul class="rattings">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-o"></i></li>
                            <li><i class="fa fa-star-o"></i></li>
                            <li class="point"><span>3.9</span></li>
                        </ul> -->
                            <!-- Course Info -->
                            <div class="course-info">
                                <span style="color: brown;font-size: 11px;font-family: monospace;"><i class="fa fa-users"></i><?php echo $data['jenis_penghuni']; ?></span>
                                <span style="color: #9d8c8c;font-size: 11px;font-family: monospace;"><i class="fa fa-clock-o"></i><?php echo $data['tgl_upload']; ?></span>
                                <span style="color: #9d8c8c;font-size: 11px;font-family: monospace;"><i class="fa fa-home"></i>Type: <?php echo $data['ukuran_kamar']; ?></span>

                            </div>
                        </div>
                        <!--/ End Course Meta -->
                    </div>
                    <!--/ End Single Course -->
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-12">
                <br>
                <div class="section-title bg">
                    <div class="icon"><i class="fa fa-share"></i></div>
                    <a href="#">
                        <!-- <div class="value off">Lebih banyak</div> -->
                    </a>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- register -->

<section class="cta">
    <div class="cta-inner overlay section" style="background-image:url('desain/images/cta-bg.jpg')" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-12">
                    <div class="text-content">
                        <h2>Hunian <span>Kami </span> Bersih, Nyaman & Aman</h2>
                        <p>Kami hadir untuk anda., agar anda bisa memiliki hunian atau tempat tinggal yang aman dan nyaman. ayoo cepat hubungi kami dan lihat kami di web.</p>
                        <!-- CTA Button -->
                        <div class="button">
                            <a href="index.php?kosan=2" class="btn white" href="contact.html">Kos-kosan</a>
                            <a href="index.php?kosan=3" class="btn white primary" href="courses.html">Kontrakan</a>
                        </div>
                        <!--/ End CTA Button -->
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-12">
                    <!-- Cta Image -->
                    <div class="cta-image">
                        <img src="desain/logo/InfoKost.png" alt="#" style="width: 300px;">
                    </div>
                    <!--/ End Cta Image -->
                </div>
            </div>
        </div>
    </div>
</section>

<!-- batas -->

<section class="faqs section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-12">
                <div class="section-title bg">
                    <h2>KOS-KOSAN <span>KONTRAKAN</span></h2>
                    <p>KAMI SIAP MENYEDIAKAN.</p>
                    <div class="icon"><i class="fa fa-question"></i></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-12">
                <div class="faq-image">
                    <img src="desain/logo/kos-kosan.png" alt="#" style="width: 400px;">
                </div>
            </div>
            <div class="col-lg-7 col-12">
                <div class="faq-main">
                    <div class="faq-content">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <!-- Single Faq -->
                            <div class="panel panel-default">
                                <div class="faq-heading" id="FaqTitle1">
                                    <h4 class="faq-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#faq1"><i class="fa fa-question"></i>Sulit dalam mencari kos/ kontrakan.</a>
                                    </h4>
                                </div>
                                <div id="faq1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="FaqTitle1">
                                    <div class="faq-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse facilisis ultricies tortor, nec sollicitudin lorem sagittis vitae. Curabitur rhoncus commodo rutrum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam nec lacus pulvinar, laoreet dolor quis, pellentesque ante. Cras nulla orci, pharetra at dictum consequat</div>
                                </div>
                            </div>
                            <!--/ End Single Faq -->
                            <!-- Single Faq -->
                            <div class="panel panel-default active">
                                <div class="faq-heading" id="FaqTitle2">
                                    <h4 class="faq-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#faq2"><i class="fa fa-question"></i>Info Kami...</a>
                                    </h4>
                                </div>
                                <div id="faq2" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="FaqTitle2">
                                    <div class="faq-body">Fitur Pencarian kosan di Kos-kosan: kami menyediakan informasi menyangkut dengan kos-kosan. serta mempermudah anda untuk mengetahui harga dan tempat kos-kosan yang murah</div>
                                </div>
                            </div>
                            <!--/ End Single Faq -->
                            <!-- Single Faq -->
                            <!-- <div class="panel panel-default">
                                <div class="faq-heading" id="FaqTitle3">
                                    <h4 class="faq-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#faq3"><i class="fa fa-question"></i>Suspendisse facilisis ultricies tortor, nec sollicitudin</a>
                                    </h4>
                                </div>
                                <div id="faq3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="FaqTitle3">
                                    <div class="faq-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse facilisis ultricies tortor, nec sollicitudin lorem sagittis vitae. Curabitur rhoncus commodo rutrum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam nec lacus pulvinar, laoreet dolor quis, pellentesque ante. Cras nulla orci, pharetra at dictum consequat</div>
                                </div>
                            </div> -->
                            <!--/ End Single Faq -->
                            <!-- Single Faq -->
                            <!-- <div class="panel panel-default">
                                <div class="faq-heading" id="FaqTitle4">
                                    <h4 class="faq-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#faq4"><i class="fa fa-question"></i>Tristique senectus et netus et malesuada fames ac turpis </a>
                                    </h4>
                                </div>
                                <div id="faq4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="FaqTitle4">
                                    <div class="faq-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse facilisis ultricies tortor, nec sollicitudin lorem sagittis vitae. Curabitur rhoncus commodo rutrum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam nec lacus pulvinar, laoreet dolor quis, pellentesque ante. Cras nulla orci, pharetra at dictum consequat</div>
                                </div>
                            </div> -->
                            <!--/ End Single Faq -->
                            <!-- Single Faq -->
                            <!-- <div class="panel panel-default">
                                <div class="faq-heading" id="FaqTitle5">
                                    <h4 class="faq-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#faq5"><i class="fa fa-question"></i>Cras nulla orci, pharetra at dictum consequat</a>
                                    </h4>
                                </div>
                                <div id="faq5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="FaqTitle5">
                                    <div class="faq-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse facilisis ultricies tortor, nec sollicitudin lorem sagittis vitae. Curabitur rhoncus commodo rutrum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam nec lacus pulvinar, laoreet dolor quis, pellentesque ante. Cras nulla orci, pharetra at dictum consequat</div>
                                </div>
                            </div> -->
                            <!--/ End Single Faq -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>