<section class="home-slider" style="height: 200px;">
    <div class="slider-active">
        <!-- Single Slider -->
        <div class="single-slider overlay">
            <div class="slider-image" style="background-image:url('desain/logo/rumah.jpg')"></div>
        </div>
        <!--/ End Single Slider -->
    </div>
</section>

<section class="courses section" style="background: #ddd6d663;padding-top: 15px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-12">
                <div class="section-title bg">
                    <h2>KONTRAKAN<span></span></h2>
                    <div class="icon"><i class="fa fa-home"></i></div>
                </div>
            </div>
        </div>
        <div class="row">

            <?php
            include "config/koneksi.php";
            $tampilkan = mysqli_query($connect, "SELECT * FROM tb_kos_kontrakan where tb_kos_kontrakan.jenis_hunian='kontrakan' order by tgl_upload desc");

            foreach ($tampilkan as $data) {

            ?>
                <div class="col-lg-3 col-md-6 col-12">
                    <!-- Single Course -->
                    <div class="single-course" style="padding: 4px;">
                        <!-- Course Head -->
                        <div class="course-head overlay">
                            <img src="adm/gambar_adm/gambar_kos_kontrakan/<?php echo $data['gambar_kos_kontrakan1']; ?>" alt="#" style="height: 190px">
                            <a target="_blank" href="index.php?kosan=4&detail=<?php echo $data['id_kos_kontrakan'];?>" class="btn white primary" style="font-size: 9px;">Booking Sekarang</a>
                        </div>
                        <!-- Course Body -->
                        <div class="course-body">
                            <div class="name-price">
                                <!-- <div class="teacher-info">
                                <img src="desain/images/author2.jpg" alt="#" style="height: 190px">
                                <h4 class="title">Jenola Protan</h4>
                            </div> -->
                                <span class="price" style="font-size: 9px;">Rp. <?php echo number_format($data['harga'], 0, ',', '.'); ?>/ <?php echo $data['jenis_sewa']; ?></span>
                            </div>
                            <!-- <h4 class="c-title"><a href="course-single.html">Learn Web Developments Course</a></h4> -->
                            <p style="padding-top: 60px;font-size: 13px;color:#093e21;font-family: monospace;"><?php echo $data['nama_kos_kontrakan']; ?></p>
                        </div>
                        <!-- Course Meta -->
                        <div class="course-meta">
                            <!-- Rattings -->
                            <!-- <ul class="rattings">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star-half-o"></i></li>
                            <li><i class="fa fa-star-o"></i></li>
                            <li class="point"><span>3.9</span></li>
                        </ul> -->
                            <!-- Course Info -->
                            <div class="course-info">
                                <span style="color: brown;font-size: 11px;font-family: monospace;"><i class="fa fa-users"></i><?php echo $data['jenis_penghuni']; ?></span>
                                <span style="color: #9d8c8c;font-size: 11px;font-family: monospace;"><i class="fa fa-clock-o"></i><?php echo $data['tgl_upload']; ?></span>
                                <span style="color: #9d8c8c;font-size: 11px;font-family: monospace;"><i class="fa fa-home"></i>Type: <?php echo $data['ukuran_kamar']; ?></span>

                            </div>
                        </div>
                        <!--/ End Course Meta -->
                    </div>
                    <!--/ End Single Course -->
                </div>
            <?php } ?>
        </div>

        <!-- <div class="row">
            <div class="col-lg-6 offset-lg-3 col-12">
                <br>
                <div class="section-title bg">
                    <div class="icon"><i class="fa fa-share"></i></div>
                    <a href="#">
                        <div class="value off">Lebih banyak</div>
                    </a>

                </div>
            </div>
        </div> -->

    </div>
</section>