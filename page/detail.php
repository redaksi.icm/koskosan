<section class="home-slider" style="height: 200px;">
    <div class="slider-active">
        <!-- Single Slider -->
        <div class="single-slider overlay">
            <div class="slider-image" style="background-image:url('desain/logo/rumah.jpg')"></div>
        </div>
        <!--/ End Single Slider -->
    </div>
</section>
<?php
$query = mysqli_fetch_array(mysqli_query($connect, "SELECT count(id_kos_kontrakan) as total_boking FROM tb_booking where status_booking='Selesai' and id_kos_kontrakan = '$_GET[detail]'"));
?>

<?php
include "config/koneksi.php";
// $tampilkan = mysqli_query($connect, "SELECT * FROM tb_kos_kontrakan where id_kos_kontrakan='$_GET[detail]'");
$tampilkan = mysqli_query($connect, "SELECT * FROM tb_kos_kontrakan inner join tb_pemilik_kos_kontrakan on tb_kos_kontrakan.id_pemilik = tb_pemilik_kos_kontrakan.id_pemilik where tb_kos_kontrakan.id_kos_kontrakan = '$_GET[detail]'");

foreach ($tampilkan as $data) {

    $hobi = explode(",", $data['jenis_fasilitas']);
?>
    <section class="events archive section" style="padding-top: 0px;">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <!-- Single Event -->
                    <div class="single-event">
                        <div class="event-image" style="height: 400px;">
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active" style="height: 100%;width: 100%">
                                        <img class="d-block w-100" src="adm/gambar_adm/gambar_kos_kontrakan/<?php echo $data['gambar_kos_kontrakan1']; ?>" alt="First slide" style="height: 400px;width: 100%">
                                    </div>
                                    <div class="carousel-item" style="height: 100%;width: 100%">
                                        <img class="d-block w-100" src="adm/gambar_adm/gambar_kos_kontrakan/<?php echo $data['gambar_kos_kontrakan2']; ?>" alt="Second slide" style="height: 400px;width: 100%">
                                    </div>
                                    <div class="carousel-item" style="height: 100%;width: 100%">
                                        <img class="d-block w-100" src="adm/gambar_adm/gambar_kos_kontrakan/<?php echo $data['gambar_kos_kontrakan3']; ?>" alt="Third slide" style="height: 400px;width: 100%">
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <!-- <div class="event-date">
                                <p>23<span>March</span></p>
                            </div> -->
                        </div>
                        <div class="event-content">
                            <h3 class="event-title"><a href="#"><?php echo $data['nama_kos_kontrakan']; ?></a></h3>
                            <button class="btn btn-default" data-toggle="modal" style="background: chocolate;"><?php echo $data['jenis_penghuni']; ?></button>
                            <button class="btn btn-warning" data-toggle="modal" style="background: #17a2b8;"><?php echo $data['jenis_hunian']; ?></button>
                            <button class="btn btn-danger" data-toggle="modal" style="background: #483e40;">Per <?php echo $data['jenis_sewa']; ?></button>
                            <br>
                            <br>
                            <p><b>Luas Kamar/ Kontrakan</b></p>
                            <br>
                            <div class="panel-body">
                                <span><img src="adm/gambar_adm/logo_f/ic_room_size.svg" alt="" width="30px"></span>
                                <span>
                                    <span style="font-size: 17px"> <?php echo $data['ukuran_kamar']; ?></span>
                                </span>
                            </div>
                            <br>
                            <br>
                            <p><b style="font-size: 14px">Fasilitas dan kamar</b></p>
                            <div class="row">
                                <?php if (in_array('Wifi Internet', $hobi) ===  true) { ?>
                                    <div class="col-lg-4 col-md-4 col-4">
                                        <img src="adm/gambar_adm/logo_f/wifi.png" alt="Ocean" style="height: 2.15em; margin-right: 1rem;" /><span>Wifi Internet</span>
                                    </div>
                                <?php } ?>
                                <?php if (in_array('Kamar Mandi', $hobi) ===  true) { ?>
                                    <div class="col-lg-4 col-md-4 col-4">
                                        <img src="adm/gambar_adm/logo_f/kamar_mandi.png" alt="Ocean" style="height: 2.15em; margin-right: 1rem;" /><span>Kamar Mandi</span>
                                    </div>
                                <?php } ?>
                                <?php if (in_array('Kasur', $hobi) ===  true) { ?>
                                    <div class="col-lg-4 col-md-4 col-4">
                                        <p><img src="adm/gambar_adm/logo_f/kasur.png" alt="Ocean" style="height: 2.15em; margin-right: 1rem;" /><span>Kasur</span></p>
                                    </div>
                                <?php } ?>
                                <?php if (in_array('AC', $hobi) ===  true) { ?>
                                    <div class="col-lg-4 col-md-4 col-4">
                                        <p><img src="adm/gambar_adm/logo_f/AC.png" alt="Ocean" style="height: 2.15em; margin-right: 1rem;" /><span>AC</span></p>
                                    </div>
                                <?php } ?>
                                <?php if (in_array('Akses Kunci 24 Jam', $hobi) ===  true) { ?>
                                    <div class="col-lg-4 col-md-4 col-4">
                                        <p><img src="adm/gambar_adm/logo_f/akses_kunci.png" alt="Ocean" style="height: 2.15em; margin-right: 1rem;" /><span>Akses Kunci 24 Jam</span></p>
                                    </div>
                                <?php } ?>
                                <div style="padding-bottom: 50px;"></div>
                                <div class="col-md-12">
                                    <p><b style="font-size: 14px">Deskripsi</b></p>
                                    <p><?php echo $data['ket_kos_kontrakan']; ?></p>
                                    <p><b style="font-size: 14px">Alamat</b></p>
                                    <p><?php echo $data['alamat_kos_kontrakan']; ?></p>
                                </div>
                            </div>

                            <hr />

                            <div class="panel-body">

                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Single Event -->
                <div class="col-md-4">
                    <div class="single-event">
                        <ul class="list-group">
                            <li class="list-group-item" style="color: #004085;background-color: #b8daff;"><b>
                                    <?php if ($data['jenis_hunian'] == 'Kontrakan') { ?>
                                        Jumlah Kontrakan : <?php echo $data['jumlah_kamar']; ?>
                                    <?php } else { ?>
                                        Jumlah Kamar : <?php echo $data['jumlah_kamar']; ?>
                                    <?php } ?>

                                </b>

                            </li>
                            <li class="list-group-item" style="color: #004085;background-color: #b8daff;">
                                <?php if ($data['jumlah_kamar'] == $query['total_boking']) { ?>
                                    <b>Status: Kamar Sudah Penuh</b>
                                <?php } else { ?>
                                    <b>Sudah Terisi : <?php echo "$query[total_boking]"; ?></b>

                                <?php } ?>

                            </li>
                            <li class="list-group-item" style="color: #f8f8f8;background-color: #3b4b61;"><b>Harga : Rp. <?php echo number_format($data['harga'], 0, ',', '.'); ?> /<?php echo $data['jenis_sewa']; ?> </b></li>
                            <li class="list-group-item"><span><img src="gambar/pemilik.png" alt="" width="30px"></span>
                                <span>
                                    <span style="font-size: 15px"> Pemilik Kos/ Kontrakan</span>
                                    <span style="font-size: 11px"> : <?php echo $data['nama_pemilik']; ?></span>
                                </span></li>
                            <li class="list-group-item">
                                <a target="_blank" href="https://wa.me/<?php echo $data['no_whatsapp']; ?>?text=Halo bapak/Ibuk <?php echo $data['nama_pemilik']; ?>, saya mau mencari kos dan kontrakan, dan saya sudah melihat di website.. " class="btn btn-info btn-block"><span class="fa fa-comments-o"></span>CHAT VIA WHATSAPP</a>
                            </li>
                            <li class="list-group-item">
                                <a href="index.php?kosan=5&id=<?php echo $data['id_kos_kontrakan']; ?>" class="btn btn-info btn-block"><span class=""></span>BOOKING SEKARANG</a>
                            </li>
                            <li class="list-group-item">
                                <span class="entry-date-time"><i class="fa fa-clock-o" aria-hidden="true"></i> upload <?php echo $data['tgl_upload']; ?> </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>