<?php
include "koneksi.php";

if (isset($_GET["kosan"])) {
    $p = $_GET["kosan"];
} else {
    $p = 1;
}
switch ($p) {

    case "1":
        require("page/beranda.php");
        break;

    case "2":
        require("page/kos-kosan.php");
        break;

    case "3":
        require("page/kontrakan.php");
        break;

    case "4":
        require("page/detail.php");
        break;

    case "5":
        require("page/booking.php");
        break;

    case "6":
        require("page/action_booking.php");
        break;

    case "7":
        require("page/invoice.php");
        break;

    case "8":
        require("page/cari_bukti_booking.php");
        break;

    case "9":
        require("page/action_cari.php");
        break;

    case "10":
        require("page/cari_cari.php");
        break;
}
